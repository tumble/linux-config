{...}: {
  imports = [
    ./desktop
    ./misc
    ./programs
    ./projects
    ./gaming
  ];
}
