{
  lib,
  inputs,
  config,
  osConfig ? null,
  ...
}: let
  cfg = osConfig.tumble-config.impermanence.home or {};
in {
  imports = [
    inputs.impermanence.nixosModules.home-manager.impermanence
  ];

  config.home = lib.mkIf (cfg.enable or false) {
    persistence."/storage${config.home.homeDirectory}" = {
      directories = let
        configHome = ".config";
        dataHome = ".local/share";
      in
        lib.mkMerge [
          [
            "Documents"
            "Music"
            "Pictures"
            "Videos"
            "Desktop"
            "${configHome}/autostart"
            "${dataHome}/applications"
          ]
          cfg.directories
        ];
      files = cfg.files;
      allowOther = true;
    };
  };
}
