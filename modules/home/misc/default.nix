{...}: {
  imports = [
    ./persist.nix
    ./state.nix
    ./home.nix
    ./nix.nix
  ];
}
