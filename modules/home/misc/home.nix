{
  lib,
  config,
  ...
}: {
  programs.home-manager = {
    enable = true;
    # path = "${xdg.configHome}/home-manager";
    #autoUpgrade = {
    #  enable = false;
    #};
  };
  home.homeDirectory = lib.mkDefault "/home/${config.home.username}";

  home.sessionVariables = {
    XDG_CONFIG_HOME = "${config.xdg.configHome}";
    XDG_STATE_HOME = "${config.xdg.stateHome}";
    XDG_CACHE_HOME = "${config.xdg.cacheHome}";
  };
}
