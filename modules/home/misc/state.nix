{
  lib,
  pkgs,
  config,
  ...
}: {
  config = {
    home.stateVersion = lib.mkDefault (builtins.throw ''Please add home.stateVersion = "${config.home.version.release}"; to the home manager config'');
  };
}
