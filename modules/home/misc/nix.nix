{pkgs,osConfig?null,lib, ...}: {
  nix = {
    package = lib.mkIf (osConfig == null) pkgs.lix;
    settings.experimental-features = ["nix-command" "flakes"];
    #channel.enable = false;
  };

  nixpkgs = lib.mkIf (!osConfig.home-manager.useGlobalPkgs ) {
  config.allowUnfree = true;
  };
}
