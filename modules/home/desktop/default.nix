{...}: {
  imports = [
    ./hyprland.nix
    ./plasma.nix
    ./cosmic.nix
    ./gnome.nix
    ./sway.nix
  ];
}
