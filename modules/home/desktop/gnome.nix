{
  lib,
  osConfig ? null,
  config,
  ...
}: {
  home = lib.mkIf ((osConfig.tumble-config.desktop or null) == "gnome") {
    persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = with config; {
        files = [
          ".config/gnome-initial-setup-done"
        ];
        directories = [
          ".local/share/gnome-settings-daemon"
          ".local/share/gnome-shell"
          ".config/goa-1.0"
          ".config/gnome-session"
          ".config/dconf"
          ".themes"
        ];
      };
    };
  };
}
