{
  config,
  lib,
  osConfig ? null,
  ...
}: {
  config = lib.mkIf ((osConfig.tumble-config.desktop or null) == "plasma") {
    home.persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = let
        configHome = ".config";
        dataHome = ".local/share";
      in {
        directories = [
          "${configHome}/gtk-3.0"
          "${configHome}/gtk-4.0"
          "${configHome}/KDE"
          "${configHome}/kde.org"
          "${configHome}/plasma-workspace"
          "${configHome}/xsettingsd"
          ".kde"

          "${dataHome}/baloo"
          "${dataHome}/dolphin"
          "${dataHome}/kactivitymanagerd"
          "${dataHome}/kate"
          "${dataHome}/klipper"
          "${dataHome}/konsole"
          "${dataHome}/kscreen"
          "${dataHome}/kwalletd"
          "${dataHome}/kxmlgui5"
          "${dataHome}/RecentDocuments"
          "${dataHome}/sddm"
        ];
        files = [
          "${configHome}/bluedevilglobalrc"
          "${configHome}/dolphinrc"
          "${configHome}/gtkrc"
          "${configHome}/gtkrc-2.0"
          "${configHome}/kded5rc"
          "${configHome}/kdeglobals"
          "${configHome}/kglobalshortcutsrc"
          "${configHome}/konsolerc"
          "${configHome}/ksmserverrc"
          "${configHome}/plasma-org.kde.plasma.desktop-appletsrc"
          "${configHome}/plasmashellrc"
          "${configHome}/systemsettingsrc"
          "${configHome}/Trolltech.conf"
          "${configHome}/user-dirs.dirs"
          "${configHome}/user-dirs.locale"

          "${dataHome}/krunnerstaterc"
          "${dataHome}/user-places.xbel"
          "${dataHome}/user-places.xbel.bak"
          "${dataHome}/user-places.xbel.tbcache"
        ];
      };
    };
  };
}
