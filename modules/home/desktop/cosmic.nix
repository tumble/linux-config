{
  lib,
  osConfig ? null,
  config,
  ...
}: {
  home = lib.mkIf ((osConfig.tumble-config.desktop or null) == "cosmic") {
    persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = with config; {
        directories = [
          ".config/cosmic"
        ];
      };
    };
  };
}
