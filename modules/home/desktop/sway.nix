{
  config,
  osConfig ? null,
  lib,
  pkgs,
  inputs,
  ...
}: {

  config = lib.mkIf ((osConfig.tumble-config.desktop or null) == "sway") {

    home.packages = with pkgs; [
    ];
    wayland.windowManager.sway = {
    enable = true;
    wrapperFeatures.gtk = true; # Fixes common issues with GTK 3 apps
    config = 
	let 
	monitorL = "DP-2";
	monitorR = "DP-3";
	in
	rec {
      modifier = "Mod4";
      terminal = "foot";
      menu = "${pkgs.dmenu}/bin/dmenu_path | ${pkgs.dmenu}/bin/dmenu | ${pkgs.findutils}/bin/xargs swaymsg exec --";
      keybindings = {
        "Ctrl+alt+t" = "exec ${terminal}";
        "alt+f4" = "kill";
        "ctrl+q" = "kill";
        "${modifier}+r" = "exec ${menu}";
          "${modifier}+f" = "fullscreen toggle";
          "${modifier}+s" = "layout stacking";
          "${modifier}+t" = "layout tabbed";
          "Ctrl+alt+delete" =
            "exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'";
      };
	  defaultWorkspace = "0";
	  assigns = {
		"dev" = [
			{class="Code";}
		];
		"game" = [
			{class="steam";}
		];
		"distraction" = [
			{app_id="librewolf";}
			{app_id="discord";}
			{app_id="firefox";}
		];

	  };
      bars = [
        {command = "eww open default";}
      ];
	  workspaceOutputAssign = [
		{output=monitorR;workspace="dev";}
		{output=monitorR;workspace="game";}
		{output=monitorL;workspace="distraction";}
	  ];
      startup = [
        # {command = "librewolf";}
        {command = "keepassxc";}
        {command = "eww open default";}
        {command = "eww update"; always = true;}
        # {command = "code";}
      ];
	  window.titlebar = false;
	  output = {
		${monitorL} = {
		pos = "0 0";
		};
		${monitorR} = {
		pos = "1920 0";
		};
	  };
      input."*".xkb_layout = "gb";
      workspaceLayout = "tabbed";
      };
  };
  programs.eww = {
    enable = true;
    configDir = ./eww;
    
  };
  };
}
