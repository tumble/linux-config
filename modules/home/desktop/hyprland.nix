{
  pkgs,
  lib,
  osConfig ? null,
  inputs,
  ...
}: {
  config = lib.mkIf ((osConfig.tumble-config.desktop or null) == "hyprland")
  {
    home.packages = with pkgs; [
      lxqt.pcmanfm-qt
      waybar
      hyprland-autoname-workspaces
      eww
    ];

    programs.kitty.enable = true;

    wayland.windowManager.hyprland = {
      enable = true;
      xwayland.enable = true;
      systemd.enable = true;

      plugins = with inputs.hyprland-plugins.packages.${pkgs.system}; [
    ];

      settings = {
        "$TERMINAL" = "foot";
        "$BROWSER" = "librewolf";

        input = {
          kb_layout = "gb";
          kb_variant = "";
          kb_model = "";
          kb_options = "";
          kb_rules = "";

          follow_mouse = 1;

          sensitivity = 0; # -1.0..1.0, 0 means no modification.
        };
        bindm = [
          "SUPER,mouse:272,movewindow"
          "SUPER,mouse:273,resizewindow"
        ];
        bind = [
          "ALT,F4, killactive,"
          ",F1, exec, $HELP"
          "CTRL_ALT, t, exec, $TERMINAL"
          "SUPER, grave, exec, $TERMINAL"
        ];
        exec-once = [
          "keepassxc"
        #   "waybar"
        ];
      };
    };
  };
}
