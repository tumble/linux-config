{
  lib,
  osConfig ? null,
  config,
  pkgs,
  ...
}: {
  imports = [
    ./vr.nix
  ];
  home = lib.mkIf (osConfig.tumble-config.gaming.enable or false) {
    persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = let
        configHome = ".config";
        dataHome = ".local/share";
      in {
        directories = [
          "${dataHome}/supertux2"
          "${dataHome}/supertuxkart"
          "${dataHome}/Cemu/"
          "${dataHome}/lutris"
          "${configHome}/rpcs3"
          "${configHome}/dolphin-emu"
          "${configHome}/retroarch"
          ".dosbox"
          "${configHome}/Chiaki"
          {
            directory = ".steam";
            method = "symlink";
          }
          {
            directory = ".wine";
            method = "symlink";
          }
          {
            directory = "Games";
            method = "symlink";
          }
          {
            directory = "${dataHome}/Steam";
            method = "symlink";
          }
          {
            directory = "${dataHome}/dolphin-emu";
            method = "symlink";
          }
          {
            directory = "${dataHome}/waydroid";
            method = "symlink";
          }
          {
            directory = "${dataHome}/PrismLauncher";
            method = "symlink";
          }
        ];
      };
    };
  };
}
