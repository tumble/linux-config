{
  lib,
  self,
  pkgs,
  osConfig ? null,
  config,
  ...
}: let
  lighthouse = let
    bs1 = "33B05B34";
    bs2 = "20642f07";
    lighthouse = lib.getExe self.packages.${pkgs.system}.lighthouse;
  in {
    on = "${lighthouse} -s ON -b ${bs1} & ${lighthouse} -s ON -b ${bs2}";
    off = "${lighthouse} -s OFF -b ${bs1} & ${lighthouse} -s OFF -b ${bs2}";
  };
in {
  config =
    lib.mkIf (osConfig.tumble-config.gaming.vr.enable or false) {
      xdg.dataFile."monado/hand-tracking-models".source = pkgs.fetchFromGitLab {
        domain = "gitlab.freedesktop.org";
        owner = "monado";
        repo = "utilities/hand-tracking-models";
        rev = "master";
        hash = "sha256-x/X4HyyHdQUxn3CdMbWj5cfLvV7UyQe1D01H93UCk+M=";
      };

      home.persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
        "/storage${config.home.homeDirectory}" = {
          directories = let
            configHome = ".config";
          in [
            "${configHome}/libsurvive"
            "${configHome}/corectrl"
          ];
        };
      };
      xdg.configFile."openxr/1/openxr_monado
.json".source = "${pkgs.monado}/share/openxr/1/openxr_monado.json";

      #       xdg.configFile."openxr/1/active_runtime.json".text = ''
      #             {
      #         	"file_format_version" : "1.0.0",
      #         	"runtime" :
      #         	{
      #         		"VALVE_runtime_is_steamvr" : true,
      #         		"library_path" : "${config.xdg.dataHome}/Steam/steamapps/common/SteamVR/bin/linux64/vrclient.so",
      #         		"name" : "SteamVR"
      #         	}
      #         }

      #       '';

      home.packages = [
        self.packages.${pkgs.system}.lighthouse
        (pkgs.writeScriptBin "openxr-switch" ''
          	#!/bin/sh
          RUNTIME=$1

          mkdir -p "${config.xdg.configHome}/openxr/1/"
          rm "${config.xdg.configHome}/openxr/1/active_runtime.json"
          rm "${config.xdg.configHome}/openvr/openvrpaths.vrpath"

          if [ "$RUNTIME" == "steamvr" ]; then
          	ln -s "${config.xdg.dataHome}/Steam/steamapps/common/SteamVR/steamxr_linux64.json" "${config.xdg.configHome}/openxr/1/active_runtime.json"
          	ln -s "${config.xdg.configHome}/openvr/openvrpaths-steamvr.vrpath" "${config.xdg.configHome}/openvr/openvrpaths.vrpath"
          fi

          if [ "$RUNTIME" == "monado" ]; then
          	ln -s "${pkgs.monado}/share/openxr/1/openxr_monado.json" "${config.xdg.configHome}/openxr/1/active_runtime.json"
          	ln -s "${config.xdg.configHome}/openvr/openvrpaths-opencomposite.vrpath" "${config.xdg.configHome}/openvr/openvrpaths.vrpath"
          fi

        '')
        (pkgs.writeScriptBin "monado-run" ''
          #!/bin/sh
          openxr-switch monado
          systemctl --user start monado.service
          ${lighthouse.on}
          sleep 5

          export PRESSURE_VESSEL_FILESYSTEMS_RW=$XDG_RUNTIME_DIR/monado_comp_ipc

          #${lighthouse.off}
          "$@"
        '')
        (pkgs.writeScriptBin "steamvr-run" ''
          #!/bin/sh
          openxr-switch steamvr
          systemctl --user stop monado.service
          sleep 5
          steam steam://launch/250820
          ${lighthouse.on}
          sleep 5

          "$@"
          #${lighthouse.off}
          systemctl --user stop monado.service
        '')
      ];

      home.shellAliases = {
        monado-start = "systemctl --user start monado.service";
        monado-stop = "systemctl --user stop monado.service";
        monado-log = "journalctl --user --follow --unit monado.service";
        monado-env = ''XR_RUNTIME_JSON="${pkgs.monado}/share/openxr/1/openxr_monado.json" PRESSURE_VESSEL_FILESYSTEMS_RW=$XDG_RUNTIME_DIR/monado_comp_ipc'';
        #lighthouse-start = lighthouse.on;
        #lighthouse-stop = lighthouse.off;
      };

      xdg.configFile."openvr/openvrpaths-steamvr.vrpath".text = ''
        {
        	"config": [
        		"${config.xdg.dataHome}/Steam/config"
        	],
        	"external_drivers": null,
        	"jsonid": "vrpathreg",
        	"log": [
        		"${config.xdg.dataHome}/Steam/logs"
        	],
        	"runtime": [
        		"${config.xdg.dataHome}/Steam/steamapps/common/SteamVR"
        	],
        	"version": 1
        }
      '';
      xdg.configFile."openvr/openvrpaths-opencomposite.vrpath".text = ''
        {
        	"config": [
        		"${config.xdg.dataHome}/Steam/config"
        	],
        	"external_drivers": null,
        	"jsonid": "vrpathreg",
        	"log": [
        		"${config.xdg.dataHome}/Steam/logs"
        	],
        	"runtime": [
        		"${pkgs.opencomposite}/lib/opencomposite"
        	],
        	"version": 1
        }
      '';

      #"${pkgs.opencomposite}/lib/opencomposite",
      #"${config.xdg.dataHome}/Steam/steamapps/common/SteamVR"
    };
}
