{
  pkgs,
  lib,
  config,
  self,
  ...
}: {
  config = {
    programs.git = {
      enable = true;
      userEmail = "tumblegamer@gmail.com";
      userName = "Tumble";
      diff-so-fancy.enable = true;
      package = pkgs.gitFull;
      extraConfig = {
        pull.rebase = true;
      };
    };
    programs.ssh = {
      enable = true;
      addKeysToAgent = "yes";
      compression = true;
      matchBlocks = lib.mkMerge [
        (builtins.mapAttrs (hostname: {
            name,
            ip,
            fqdn,
            ssh,
            ...
          }: {
            hostname = "tumble.ficial.net";
            port = ssh;
            #identityFile = "~/.ssh/${name}";
          })
          self.hostsSet)
        {
          jirachi = {
            user = "deck";
            hostname = lib.mkForce "192.168.0.33";
            port = lib.mkForce 22;
          };

          sarpnt = {
            hostname = "sarpnt.ficial.net";
            port = 7131;
            #identityFile = "~/.ssh/sarpnt";
          };

          "github.com" = {
            user = "git";
            #identityFile = "~/.ssh/github";
          };

          "gitlab.com" = {
            user = "git";
            #identityFile = "~/.ssh/gitlab";
          };

          "git.disroot.org" = {
            user = "git";
            #identityFile = "~/.ssh/git.disroot.org";
          };

          vyos = {
            hostname = "bmod.tf";
            user = "vyos";
            #identityFile = "~/.ssh/bmod";
          };

          bmod = {
            hostname = "bmod.tf";
            port = 2202;
            #identityFile = "~/.ssh/bmod";
          };
          "*" = {
            # user = "tumble";
            #identitiesOnly = true;
            forwardX11 = false;
          };
        }
      ];
    };
  };
}
