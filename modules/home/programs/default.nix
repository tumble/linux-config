{
  pkgs,
  self,
  ...
}: {
  imports = [
    ./foot.nix
    ./git.nix
    ./kdeconnect.nix
    ./jellyfin-shim.nix
  ];
  home.packages = with pkgs; [
    gnupg

    mate.engrampa

    self.packages.${pkgs.system}.tumble-update
  ];

  xdg.mimeApps = {
    enable = true;
    defaultApplications = let
      web = "librewolf.desktop";
      files = "pcmanfm-qt.desktop";
      terminal = "foot.desktop";
      image = "xviewer.desktop";
      video = "mpv.desktop";
      audio = video;
      pdf = "draw.desktop";
      archive = "engrampa.desktop";
      mail = "org.gnome.Evolution.desktop";
      calendar = mail;
      contacts = mail;
      n64 = "com.github.Rosalie241.RMG.desktop";
      snes = "bsnes.desktop";
      nes = "nestopia.desktop";
      ds = "net.kuribo64.melonDS.desktop";
      wii = "dolphin-emu.desktop";
      wiiu = "info.cemu.Cemu.desktop";
      windows = "wine.desktop";
    in {
      #browser
      "x-scheme-handler/https" = web;
      "x-scheme-handler/http" = web;
      "text/html" = web;
      # file manager
      "inode/directory" = files;
      #terminal
      "terminal" = terminal;
      #image
      "image/jpeg" = image;
      "image/png" = image;
      "image/x-icns" = image;
      #video
      "video/mp4" = video;
      "video/webm" = video;
      #audio
      "audio/mpeg" = audio;
      "audio/x-speex" = audio;
      #pdf
      "application/pdf" = pdf;
      #archive
      "application/zip" = archive;
      "application/x-tar" = archive;
      "application/x-7z-compressed" = archive;
      "application/x-rar" = archive;
      "application/x-lz4-compressed-tar" = archive;
      "application/x-bcpio" = archive;
      "application/x-sv4cpio" = archive;
      "application/x-sv4crc" = archive;
      "application/x-xar" = archive;
      #mail
      "x-scheme-handler/mailto" = mail;
      #calendar
      "x-scheme-handler/webcal" = calendar;
      "text/calendar" = calendar;
      #contacts
      "text/vcard" = contacts;

      "application/x-nintendo-ds-rom" = ds;
      "application/x-wbfs" = wii;
      "application/x-wua" = wiiu;
      "application/x-nes-rom" = nes;
      "application/vnd.nintendo.snes.rom" = snes;
      "application/x-n64-rom" = n64;

      # windows
      "application/x-ms-dos-executable" = windows;
      "application/x-msi" = windows;
    };
  };
}
# nix-store --query --tree /nix/store/$(ls -1 /nix/store| grep "Spam")

