{...}: {
  programs.foot = {
    enable = true;
    settings = {
      main = {
        term = "xterm-256color";

        font = "monospace:size=14";
        dpi-aware = "yes";
      };
      tweak = {
        font-monospace-warn = "no";
      };

      mouse = {
        hide-when-typing = "yes";
      };
    };
  };
}
