{
  pkgs,
  lib,
  config,
  osConfig ? null,
  ...
}: {
  # services.kdeconnect = {
  # 	enable = true;
  # 	#indicator = true;
  # };
  home = {
    packages = with pkgs; [sshfs];
    persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = {
        directories = with config; [
          ".config/kdeconnect"
        ];
      };
    };
  };
}
