{
  pkgs,
  lib,
  config,
  osConfig ? null,
  ...
}: let
  imp = osConfig.tumble-config.impermanence.home or {};
in {
  home = {
    packages = with pkgs; [
      jellyfin-mpv-shim
    ];
    persistence = lib.mkIf (imp.enable or false) {
      "/storage${config.home.homeDirectory}" = {
        directories = [
          ".config/jellyfin-mpv-shim/"
        ];
      };
    };
  };
}
