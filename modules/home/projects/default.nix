{...}: {
  imports = [
    ./android.nix
    ./gitgui.nix
    ./keepass.nix
    ./logseq.nix
    ./rust.nix
    ./unity.nix
    ./vscode.nix
    ./evolution.nix
    ./pnpm.nix
    ./sops.nix
	./syncthing.nix
  ];
}
