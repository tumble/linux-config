{
  lib,
  osConfig ? null,
  config,
  pkgs,
  ...
}: {
  home = lib.mkIf (osConfig.tumble-config.projects.enable or false) {
    persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = {
        directories = let
          dataHome = ".local/share";
        in [
          "${dataHome}/rustup"
          "${dataHome}/cargo"
        ];
      };
    };

    packages = with pkgs; [rustup gcc];

    sessionVariables = {
      RUSTUP_HOME = "${config.xdg.dataHome}/rustup";
      CARGO_HOME = "${config.xdg.dataHome}/cargo";
    };
  };
}
