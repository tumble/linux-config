{
  pkgs,
  config,
  lib,
  osConfig ? null,
  ...
}: {
  config = lib.mkIf (osConfig.tumble-config.projects.enable or false) {
    home.persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = {
        directories = with config; [
          ".cache/keepassxc"
          ".config/keepassxc"
        ];
      };
    };

    xdg.configFile = {
      "kwalletrc" = {
        recursive = true;
        text = ''
          [Wallet]
          Enabled=false
        '';
      };
    };
  };
}
