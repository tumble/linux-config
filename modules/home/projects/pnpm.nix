{
  lib,
  osConfig ? null,
  config,
  pkgs,
  ...
}: {
  home = lib.mkIf (osConfig.tumble-config.projects.enable or false) {
    persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = {
        directories = let
          dataHome = ".local/share";
        in [
          "${dataHome}/pnpm"
        ];
      };
    };

    packages = with pkgs; [nodejs pnpm 
    #biome
    ];

    sessionVariables = {
      PNPM_HOME = "${config.xdg.dataHome}/pnpm";
    };
  };
}
