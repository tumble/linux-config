{
  lib,
  osConfig ? null,
  config,
  ...
}: {
  home = lib.mkIf (osConfig.tumble-config.projects.enable or false) {
    persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = {
        directories = let
          configHome = ".config";
          dataHome = ".local/share";
        in [
          "${configHome}/GitHub Desktop"
          "${configHome}/gittyup.github.com"
          "${dataHome}/Gittyup"
          "GitHub"
        ];
      };
    };
  };
}
