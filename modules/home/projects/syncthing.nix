{
  lib,
  osConfig ? null,
  config,
  pkgs,
  ...
  }:{
    config = lib.mkIf (osConfig.tumble-config.projects.enable or true) {

        services.syncthing = {
            enable = true;
            #tray.enable = true;
        };
		
		home.packages = with pkgs; [ syncthingtray ];
  };
}