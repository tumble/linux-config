{
  lib,
  osConfig ? null,
  config,
  ...
}: {
  home = lib.mkIf (osConfig.tumble-config.projects.enable or false) {
    persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = {
        directories = with config; [
          ".config/evolution"
          ".cache/evolution"
          ".local/share/evolution"
        ];
      };
    };
  };
}
