{
  lib,
  osConfig ? null,
  config,
  ...
}: {
  home = lib.mkIf (osConfig.tumble-config.projects.enable or false && osConfig.services.unity.enable or false) {
    persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
      "/storage${config.home.homeDirectory}" = {
        directories = let
          configHome = ".config";
        in [
          "${configHome}/unity3d"
          "${configHome}/unityhub"
          "Unity"
        ];
      };
    };
  };
}
