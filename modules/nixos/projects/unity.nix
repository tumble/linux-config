{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: let
  unitypkgs = import inputs.unity {
    system = pkgs.system;
    config.allowUnfree = true;
  };
in {
  options.tumble-config.projects.unity = {
    enable = lib.mkEnableOption "Unity";
  };
  config = lib.mkIf config.tumble-config.projects.unity.enable {
    environment.systemPackages = with pkgs; [
      unityhub
      mono
      dotnet-sdk
      #unitypkgs.unityhub
    ];
    #programs.nix-ld.enable = true;
  };
}
