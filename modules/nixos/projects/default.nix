{
  lib,
  config,
  pkgs,
  ...
}: {
  imports = [
    ./unity.nix
    ./obs.nix
  ];

  options.tumble-config.projects.enable = lib.mkEnableOption "Enable project work";

  config = lib.mkIf config.tumble-config.projects.enable {
    programs.adb.enable = true;

    boot.kernelModules = ["uvcvideo" "v4l2_common" "bttv"];

    programs = {
      git = {
        enable = true;
        lfs.enable = true;
        package = pkgs.gitFull;
      };
      # browsers
      ladybird.enable = true;
      ssh.startAgent = true;
      thunderbird = {
        enable = true;
        preferencesStatus = true;
      };
      kclock.enable = true;
    };


    services.gnome.gnome-keyring.enable = lib.mkForce false;

    environment.systemPackages = with pkgs; [
      libreoffice-qt
      #   logseq

      keepassxc
      lapce
      vscode
      #   ardour
      # blender

      gitflow

      gnome-font-viewer
      krita

      github-desktop
      #   gittyup

      epiphany
      firefox
      ungoogled-chromium

      tumble-fmt
      nixpkgs-fmt

      heimdall-gui
      pmbootstrap

      musescore

      v4l-utils
      ffmpeg

      pmbootstrap
      heimdall-gui
      heimdall

      lomiri.lomiri-sounds
    ];
  };
}
