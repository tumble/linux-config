{
  lib,
  config,
  pkgs,
  self,
  ...
}:
let
cfg = config.tumble-config.htpc;
in
{
  config = lib.mkIf cfg.android{
    environment.systemPackages = with pkgs; [
      weston
      cage
    ];
  };
}
