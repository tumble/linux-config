{
  lib,
  config,
  pkgs,
  ...
}: let
cfg = config.tumble-config.htpc;
  kodi = pkgs.kodi-wayland.withPackages (pkgs:
    with pkgs; [
      sendtokodi
      jellyfin
      pvr-hdhomerun

      inputstream-adaptive

      steam-controller
    ]);
in {
  config = lib.mkIf cfg.enable {
    environment.systemPackages = [kodi];

    services = lib.mkIf (!config.tumble-config.gaming.enable) {
      xserver = {
        enable = lib.mkDefault true;

        displayManager.autoLogin = {
            enable = true;
            user = cfg.user;
        };

        desktopManager.kodi = {
          enable = !config.tumble-config.gaming.enable;
          package = kodi;
        };
        displayManager.lightdm = {
          autoLogin.timeout = 0;
          greeter.enable = false;
        };
      };
    };

    networking.firewall = {
      allowedTCPPorts = [8080];
      allowedUDPPorts = [8080];
    };
  };
}
