{
  lib,
  config,
  pkgs,
  self,
  ...
}:

let
cfg = config.tumble-config.htpc;
in
{
  config = lib.mkIf cfg.dvd-drive {
    environment.systemPackages = with pkgs; [
      self.packages.${pkgs.system}.autorun-dvd

      (
        makeDesktopItem
        {
          name = "Disk Drive";
          desktopName = "disk-drive";
          exec = "${nix}/bin/nix-shell -p util-linux-udisks --command ${self.packages.${pkgs.system}.autorun-dvd}/bin/autorun-dvd";
        }
      )
    ];
  };
}
