{
  lib,
  config,
  self,
  pkgs,
  ...
}:
let
cfg = config.tumble-config.htpc;
in
{
  imports = [
    ./kodi.nix
    ./dvd-drive.nix
    ./androidtv.nix
    ./steamos.nix
  ];

  options.tumble-config.htpc = {
    enable = lib.mkEnableOption "Enable HTPC";
    dvd-drive = lib.mkEnableOption "Enable CD Drive";
    android = lib.mkEnableOption "Enable Android TV";
      steamdeck = lib.mkEnableOption "Enable SteamDeck";
    amd-gpu = lib.mkEnableOption "AMD GPU";
    user = lib.mkOption {
        type = lib.types.str;
        description = "steamos user";
        default = "tv";
    };
  };

  config = lib.mkIf cfg.enable {
    tumble-config.nm = true;

    tumble-config.gaming.enable = lib.mkIf cfg.steamdeck (lib.mkForce true);
    tumble-config.htpc.amd-gpu = lib.mkIf cfg.steamdeck (lib.mkForce true);

    users.users.tv = {
      isNormalUser = true;
      extraGroups = ["wheel"];
    };

    services.getty.autologinUser = cfg.user;

    services = {
      logind.powerKey = "suspend";
      udev.packages = [
        self.packages.${pkgs.system}.mudkip-wechip
      ];
    };
  };
}
