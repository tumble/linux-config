{
  config,
  lib,
  inputs,
  ...
}:
let
cfg = config.tumble-config.htpc;
in
{

  imports = [
    inputs.jovian.nixosModules.jovian
  ];
  config = lib.mkIf (config.tumble-config.gaming.enable && cfg.enable) {
    
    programs.steam.gamescopeSession.enable = true;
    jovian = {
      steam = {
        enable = true;
        desktopSession = config.tumble-config.desktop;
        autoStart = true;
        updater.splash = "bgrt";
        user = config.tumble-config.htpc.user;
      };
      decky-loader.enable = true;

      devices.steamdeck.enable = cfg.steamdeck;
      hardware.has.amd.gpu = cfg.amd-gpu;
    };
  };
}
