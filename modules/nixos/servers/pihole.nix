{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.tumble-config.servers.pihole;
  imp = config.tumble-config.impermanence;
in {
  options.tumble-config.servers.pihole = {
    enable = lib.mkEnableOption "Enable PiHole";

    dockerImage = mkOption {
      type = types.str;
      default = "pihole/pihole:latest";
      description = "Docker image for Pi-hole.";
    };

    webPassword = mkOption {
      type = types.str;
      default = "admin";
      description = "Password for the Pi-hole admin interface.";
    };

    webPort = mkOption {
      type = types.int;
      default = 8070;
      description = "Port for the Pi-hole web interface.";
    };

    dnsPort = mkOption {
      type = types.int;
      default = 53;
      description = "DNS port for Pi-hole.";
    };
  };
  config = mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = [cfg.webPort cfg.dnsPort 67 68];
    networking.firewall.allowedUDPPorts = [cfg.dnsPort 67 68];
    virtualisation.docker.enable = lib.mkForce true;
    systemd.services.pihole = {
      description = "Pi-hole in Docker";
      after = ["docker.service"];
      requires = ["docker.service"];
      serviceConfig = {
        ExecStart = ''
              ${pkgs.docker}/bin/docker run \
                --name pihole \
          --net=host \
                -e TZ="UTC" \
                -e WEBPASSWORD=${toString cfg.webPassword} \
                -e WEB_PORT=${toString cfg.webPort} \
                -p ${toString cfg.webPort}:${toString cfg.webPort} \
                -p ${toString cfg.dnsPort}:${toString cfg.dnsPort}/tcp \
                -p ${toString cfg.dnsPort}:${toString cfg.dnsPort}/udp \
          -p 67:67 \
                -p 68:68 \
                --dns=127.0.0.1 \
                --dns=9.9.9.9 \
                --cap-add=NET_ADMIN \
                -v /etc/pihole:/etc/pihole \
                -v /etc/dnsmasq.d:/etc/dnsmasq.d \
                --restart=unless-stopped \
                ${cfg.dockerImage}
        '';
        ExecStop = "${pkgs.docker}/bin/docker stop pihole";
        ExecReload = "${pkgs.docker}/bin/docker restart pihole";
        Restart = "always";
      };
      wantedBy = ["multi-user.target"];
    };

    environment.persistence.${imp.path} = lib.mkIf imp.enable {
      directories = [
        "/etc/pihole"
      ];
    };
  };
}
