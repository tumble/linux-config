{
  config,
  lib,
  pkgs,
  ...
}: {
  options.tumble-config.servers.home-assistant.enable = lib.mkEnableOption "Enable Home Assistant";
  config = lib.mkIf config.tumble-config.servers.home-assistant.enable {
    services.home-assistant = {
      enable = true;
      extraComponents = [
        "analytics"
        "google_translate"
        "met"
        "radio_browser"
        "shopping_list"
        "homekit"
        "cast"
        "nest"
        "generic"
        "kodi"
        "youtube"
        "androidtv_remote"
        # Recommended for fast zlib compression
        # https://www.home-assistant.io/integrations/isal
        "isal"
        "jellyfin"
        "metoffice"
      ];
      config = {
        # Includes dependencies for a basic setup
        # https://www.home-assistant.io/integrations/default_config/
        default_config = {};
      };
    };

    networking.firewall.allowedTCPPorts = [
      config.services.home-assistant.config.http.server_port
    ];
  };
}
