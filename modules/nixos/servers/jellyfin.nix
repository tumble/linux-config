{
  config,
  lib,
  pkgs,
  ...
}: {
  options.tumble-config.servers.jellyfin.enable = lib.mkEnableOption "Enable Jellyfin";
  config = lib.mkIf config.tumble-config.servers.jellyfin.enable {
    services.jellyfin = {
      enable = true;
      openFirewall = true;
    };

    networking.firewall = {
      allowedTCPPorts = [8096 8920];
      allowedUDPPorts = [1900 7359];
    };

    environment.systemPackages = [
      pkgs.jellyfin
      pkgs.jellyfin-ffmpeg
    ];

    nixpkgs.overlays = with pkgs; [
      (
        final: prev: {
          jellyfin-web = prev.jellyfin-web.overrideAttrs (finalAttrs: previousAttrs: {
            installPhase = ''
              runHook preInstall

              # this is the important line
              sed -i "s#</head>#<script src=\"configurationpage?name=skip-intro-button.js\"></script></head>#" dist/index.html

              mkdir -p $out/share
              cp -a dist $out/share/jellyfin-web

              runHook postInstall
            '';
          });
        }
      )
    ];
  };
}
