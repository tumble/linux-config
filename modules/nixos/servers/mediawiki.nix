{
  config,
  lib,
  pkgs,
  ...
}: {
  options.tumble-config.servers.mediawiki.enable = lib.mkEnableOption "Enable MediaWiki";
  config = lib.mkIf config.tumble-config.servers.mediawiki.enable {
    services.mediawiki = {
      enable = true;
      name = "Ficial Wiki Family";
      httpd.virtualHost = {
        hostName = "wiki.tumble.ficial.net";
        adminAddr = "tumble@ficial.net";
      };
      # Administrator account username is admin.
      # Set initial password to "cardbotnine" for the account admin.
      passwordFile = pkgs.writeText "password" "cardbotnine";
      extraConfig = ''
        # Disable anonymous editing
        $wgGroupPermissions['*']['edit'] = false;
      '';

      extensions = {
        # some extensions are included and can enabled by passing null
        VisualEditor = null;
      };
      httpd.virtualHost.listen = [
        {
          ip = "127.0.0.1";
          port = 3003;
          ssl = false;
        }
      ];
    };

    services.phpfpm.pools.mediawiki.phpOptions = ''
      upload_max_filesize = 10M
      post_max_size = 15M
    '';
  };
}
