{
  config,
  lib,
  pkgs,
  inputs,
  ...
}: let
  cfg = config.tumble-config.servers.game-with-chat;
in {
  options.tumble-config.servers.game-with-chat = {
    enable = lib.mkEnableOption "Enable Game wITH cHAT";
    port = lib.mkOption {
      default = 3000;
    };
    package = lib.mkOption {
      default = inputs.gwc.defaultPackage.${pkgs.system};
    };
  };
  config = lib.mkIf cfg.enable {
    # nixpkgs.overlays = [ (final: prev: {
    # 	game-with-chat = inputs.gwc.defaultPackage.${pkgs.system};
    # })];

    systemd.services.game-with-chat = {
      enable = true;
      after = ["network.target"];
      wantedBy = ["default.target"];
      description = "Game with chat server";
      environment = {
        PORT = toString cfg.port;
      };
      serviceConfig = {
        WorkingDirectory = "${cfg.package}/lib/node_modules/gwc";
      };
      script = "${cfg.package}/bin/gwc";
    };

    services.caddy.virtualHosts."http://gwc.tumble.ficial.net".extraConfig = ''
      reverse_proxy http://localhost:${toString cfg.port}
    '';

    networking.firewall = {
      enable = lib.mkForce true;
      allowedTCPPortRanges = [
        {
          from = cfg.port;
          to = cfg.port;
        }
      ];
      # allowedUDPPortRanges = [
      # 	{ from = ${cfg.port}; to = ${cfg.port}; }
      # ];
    };
  };
}
