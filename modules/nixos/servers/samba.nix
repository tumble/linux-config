{
  config,
  lib,
  pkgs,
  ...
}: {
  options.tumble-config.servers.samba.enable = lib.mkEnableOption "Enable Samba";
  config = lib.mkIf config.tumble-config.servers.samba.enable {
    services.samba = {
      enable = true;
      #securityType = "user";
      openFirewall = true;
      # extraConfig =
      # let
      # 	hostname = config.networking.hostName;
      # in
      # ''
      # 	workgroup = WORKGROUP
      # 	server string = ${hostname}
      # 	netbios name = ${hostname}
      # 	browseable = yes
      # 	#use sendfile = yes
      # '';

      settings = let
        hostname = config.networking.hostName;
      in {
        global = {
          workgroup = "WORKGROUP";
          "server string" = hostname;
          "netbios name" = hostname;
          browseable = "yes";
          #use sendfile = yes;
        };
        archive = {
          path = "/archive";
          browseable = "yes";
          writeable = "yes";
          "read only" = "no";
          "create mask" = "0776";
          "directory mask" = "0776";
          "force group" = "archive";
        };
        homes = {
          browseable = "yes";
          writeable = "yes";
          "read only" = "no";
          "valid users" = "%S";
        };
      };
    };

    networking.firewall.allowedTCPPorts = [445 139];
    networking.firewall.allowedUDPPorts = [137 138];

    services.samba-wsdd = {
      enable = true;
      openFirewall = true;
    };

    # TODO put comments above relevant lines and remove "^^"
    services.avahi = {
      publish.enable = true;
      publish.userServices = true;
      # ^^ Needed to allow samba to automatically register mDNS records (without the need for an `extraServiceFile`
      nssmdns4 = true;
      # ^^ Not one hundred percent sure if this is needed- if it aint broke, don't fix it
      enable = true;
      openFirewall = true;
    };
  };
}
