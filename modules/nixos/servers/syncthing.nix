{
  config,
  lib,
  pkgs,
  ...
}: {
  options.tumble-config.servers.syncthing.enable = lib.mkEnableOption "Enable Syncthing";
  config = lib.mkIf config.tumble-config.servers.syncthing.enable {
    services.syncthing = {
      enable = true;
      user = "syncthing";
      overrideFolders = false;
      overrideDevices = false;
      dataDir = "/var/lib/syncthing/data";
      configDir = "/var/lib/syncthing/config";
      databaseDir = "/var/lib/syncthing/database";
      openDefaultPorts = true;
      guiAddress = "0.0.0.0:8384";

      settings = {
        options.urAccepted = -1;
      };
    };

    users.users = {
      syncthing = {
        isSystemUser = true;
        extraGroups = ["syncthing"];
        home = lib.mkForce "/var/lib/syncthing";
      };
    };

    networking.firewall = {
      allowedTCPPorts = [8384];
      allowedUDPPorts = [8384];
    };
  };
}
