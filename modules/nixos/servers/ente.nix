{
  config,
  lib,
  pkgs,
  ...
}: {
  options.tumble-config.servers.ente.enable = lib.mkEnableOption "Enable Radicale";
  config = lib.mkIf config.tumble-config.servers.ente.enable {
    # services.museum = {
    # 	enable = true;
    # 	settings = {
    # 		# TODO add example
    # 	};
    # 	credentialsFile = "/path/to/secrets/creds.yaml";
    # environmentFile = "/path/to/env";
    # };

    # services.nginx = {
    # 	enable = true;
    # 	virtualHosts."photos.example.com" = {
    # 		enableACME = true;
    # 		forceSSL = true;
    # 		root = pkgs.ente-web;
    # 	};
    # };
  };
}
