{
  config,
  lib,
  pkgs,
  ...
}: {
  options.tumble-config.servers.peertube.enable = lib.mkEnableOption "Enable Peertube";
  config = lib.mkIf config.tumble-config.servers.peertube.enable (let
    postgresqlPass = config.sops.secrets.peertube-postgresql.path;
    redisPass = config.sops.secrets.peertube-redis.path;
    secretsPass = config.sops.secrets.peertube-secret.path;
  in {
    sops.secrets = let
      basicStuff = {
        format = "json";
        mode = "0440";
        owner = "peertube";
        group = "postgres";
      };
    in {
      peertube-redis = basicStuff;
      peertube-postgresql = basicStuff;
      peertube-secret = basicStuff;
    };

    networking.extraHosts = ''
      127.0.0.1 tube.tumble.ficial.net
    '';

    services = {
      peertube = {
        enable = true;
        localDomain = "tube.ficial.net";
        enableWebHttps = false;
        listenWeb = 443;
        listenHttp = 9000;
        secrets.secretsFile = secretsPass;
        database = {
          host = "127.0.0.1";
          name = "peertube";
          user = "peertube";
          passwordFile = postgresqlPass;
        };
        redis = {
          host = "127.0.0.1";
          port = 31638;
          passwordFile = redisPass;
        };
        settings = {
          listen.hostname = "0.0.0.0";
          instance.name = "FicialTube";
        };
      };

      postgresql = {
        enable = true;
        enableTCPIP = true;
        ensureDatabases = ["peertube"];
        ensureUsers = [
          {
            name = "peertube";
            ensureDBOwnership = true;
          }
        ];
        # 	authentication = pkgs.lib.mkOverride 10 ''
        # 	#type database  DBuser  auth-method optional_ident_map
        #  	 hostnossl peertube peertube 127.0.0.1/32 md5
        # 	local sameuser  all     peer        map=superuser_map
        # '';
        initialScript = pkgs.writeText "postgresql_init.sql" ''
          CREATE EXTENSION IF NOT EXISTS pg_trgm;
          CREATE EXTENSION IF NOT EXISTS unaccent;
        '';
      };

      redis.servers.peertube = {
        enable = true;
        bind = "0.0.0.0";
        port = 31638;
      };
    };

    networking.firewall = {
      allowedTCPPorts = [9000];
    };

    systemd.services.init-peertube = {
      description = "Initialize databases with secrets";
      after = ["network.target" "sops-decrypt.service" "postgresql.service"];
      requiredBy = ["peertube.service"];
      serviceConfig = {
        Type = "oneshot";
        User = "postgres";
      };
      path = with pkgs; [postgresql_16 redis replace-secret];
      wantedBy = ["multi-user.target"];

      serviceConfig = {
        RuntimeDirectory = "postgresql-setup";
        RuntimeDirectoryMode = "700";
      };
      script = ''
        #!/bin/sh

        # Read SOPS secrets
        POSTGRES_PASS=$(cat ${postgresqlPass})
        REDIS_PASS=$(cat ${redisPass})

        # Initialize PostgreSQL
        install --mode 600 ${./peertube.sql} ''$RUNTIME_DIRECTORY/init.sql
        ${pkgs.replace-secret}/bin/replace-secret @PEERTUBE_PASSWORD@ ${postgresqlPass} ''$RUNTIME_DIRECTORY/init.sql
        psql peertube --file "''$RUNTIME_DIRECTORY/init.sql"

        # Initialize Redis
        redis-cli -p 31638 -a "$REDIS_PASS" CONFIG SET requirepass "$REDIS_PASS"|true
      '';
    };
  });
}
