{
  config,
  lib,
  pkgs,
  ...
}: {
  options.tumble-config.servers.radicale.enable = lib.mkEnableOption "Enable Radicale";
  config = lib.mkIf config.tumble-config.servers.radicale.enable {
    services.radicale = {
      enable = true;
      settings = {
        server.hosts = ["0.0.0.0:5232"];
      };
    };
    networking.firewall.allowedTCPPorts = [5232];
  };
}
