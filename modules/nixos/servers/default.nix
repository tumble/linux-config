{
  config,
  lib,
  pkgs,
  ...
}: {
  imports = [
    ./jellyfin.nix
    ./samba.nix
    ./radicale.nix
    ./mediawiki.nix
    ./gwc.nix
    ./home-assistant.nix
    ./ente.nix
    ./syncthing.nix
    ./ssh.nix
    ./pihole.nix
    ./peertube.nix
  ];
}
