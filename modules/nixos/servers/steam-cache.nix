{
  config,
  lib,
  pkgs,
  ...
}: {
  options.tumble-config.servers.steam-cache.enable = lib.mkEnableOption "Enable Steam Cache";
  config = lib.mkIf config.tumble-config.servers.steam-cache.enable {
    #     services.caddy = {
    #   enable = true;
    #   virtualHosts = {
    #     "cs.steampowered.com" = {
    #         root="/var/cache/steam/cs";

    #     };

    #   };
    # };
  };
}
