{
  config,
  lib,
  pkgs,
  options,
  self,
  inputs,
  ...
}: let
  localhost = config.networking.hostName;
  my = builtins.getAttr localhost self.hostsSet;
in {
  imports = [
    inputs.ficial-config.outPath
  ];
  options.tumble-config.servers.ssh.enable = lib.mkEnableOption "Enable SSH";
  config = lib.mkIf config.tumble-config.servers.ssh.enable {
    official.systemBase = lib.mkIf (config.official.systemBase.owner != null) {
      sshPorts = lib.mkForce [my.ssh];
      moshPorts = {
        from = lib.mkForce my.moshStart;
        to = lib.mkForce my.moshEnd;
      };
    };
  };
}
