{
  pkgs,
  lib,
  config,
  ...
}: let
  cfg = config.virtualisation.libvirtd;
  types = lib.types;
  virsh = "${pkgs.libvirt}/bin/virsh";
  diskTypes = {
    qcow2 = "qcow2";
  };
  mkYesNo = b:
    if b
    then "yes"
    else "no";
  mkOnOff = b:
    if b
    then "ok"
    else "off";
  mkOptDef = default: lib.mkOption {inherit default;};
  mkOptType = type: lib.mkOption {inherit type;};
  mkOptNamed = options:
    lib.mkOption {
      default = {};
      type = with types; attrsOf (submodule [{inherit options;}]);
    };
  mapSetToString = string: set: lib.concatStringsSep "\n" (lib.mapAttrsToList string set);
  diskOptions = {
    type = mkOptDef "file";
    device = mkOptDef "disk";
    size = mkOptDef "1K";
    driver = {
      name = mkOptDef "qemu";
      type = mkOptDef "raw";
      discard = lib.mkOption {
        default = "unmap";
        type = types.nullOr types.str;
      };
    };
    source = mkOptType types.str;
    bus = mkOptDef "virtio";
    extraConfig = mkOptDef "";
    boot = lib.mkOption {
      default = null;
      type = types.nullOr types.int;
    };
  };
  interfaceOptions = {
    mac = lib.mkOption {
      default = null;
      type = types.nullOr types.str;
    };
    type = mkOptDef "network";
    macvtapType = mkOptDef "bridge";
    model = mkOptDef "virtio";
    extraConfig = mkOptDef "";
  };
  networkOptions = {
    extraConfig = mkOptDef "";
    autostart = mkOptDef true;
  };
  domainOptions = {
    extraConfig = mkOptDef "";
    extraSetup = mkOptDef "";
    type = mkOptDef "kvm";
    osType = mkOptDef "hvm";
    cpu = {
      mode = mkOptDef "host-passthrough";
      sockets = mkOptDef 1;
      cores = mkOptDef 1;
      threads = mkOptDef 1;
    };
    memory = mkOptDef 1;
    os = {
      type = mkOptDef "hvm";
      machine = mkOptDef "q35";
      uefi = with types;
        lib.mkOption {
          type = types.either bool str;
          default = false;
        };
      boot = mkOptDef "hd";
      arch = mkOptDef "x86_64";
      bootmenu = mkOptDef false;
      extraConfig = mkOptDef "";
    };
    devices = {
      graphics = {
        spice = {
          enable = lib.mkEnableOption "enable spice server";
          port = mkOptDef (-1);
          tlsPort = mkOptDef (-1);
          autoPort = mkOptDef true;
          imageCompression = lib.mkEnableOption "enable image compression";
        };
        vnc = {
          enable = lib.mkEnableOption "enable vnc server";
          port = mkOptDef (-1);
          listen = mkOptDef "127.0.0.1";
        };
      };
      video = mkOptDef "none";
      sound = lib.mkOption {
        default = ["ich9"];
        type = types.listOf types.str;
      };
      disks = mkOptNamed diskOptions;
      network = mkOptNamed interfaceOptions;
      extraConfig = mkOptDef "";
    };
    features = {
      acpi = mkOptDef true;
      apic = mkOptDef true;
      vmport = mkOptDef false;
      extraConfig = mkOptDef "";
    };
  };
  mkUUID = name: let
    hash = builtins.hashString "sha1" name;
    substr = a: b: let
      start = a * 2;
      len = (b - a + 1) * 2;
    in
      builtins.substring start len hash;
    time_low = substr 0 3;
    time_mid = substr 4 5;
    time_hi = substr 6 7;
    version = 5;
    clock_seq_hi_and_reserved = substr 8 8;
    clock_seq_low = substr 9 9;
    node = substr 10 15;
  in "${time_low}-${time_mid}-${time_hi}-${clock_seq_hi_and_reserved}${clock_seq_low}-${node}";
  #<address type='drive' controller='0' bus='0' target='0' unit='1'/>
  mkDomain = name: {
    type ? "kvm",
    osType ? "hvm",
    uuid ? mkUUID name,
    vcpu ? cpu.sockets * cpu.cores * cpu.threads,
    cpu ? {
      sockets = 1;
      cores = 1;
      threads = 1;
    },
    features ? {},
    devices ? {},
    os ? {},
    memory ? 1,
    extraOS ? "",
    extraConfig ? "",
    ...
  }: let
    uefiLoader =
      if (builtins.isBool os.uefi)
      then "/run/libvirt/nix-ovmf/OVMF_CODE.fd"
      else os.uefi;
    spiceServer = let
      cfg = devices.graphics.spice;
    in
      if cfg.enable
      then ''
         <graphics type="spice" port="${builtins.toString cfg.port}" tlsPort="${builtins.toString cfg.tlsPort}" autoport="${mkYesNo cfg.autoPort}">
          <image compression="${mkOnOff cfg.imageCompression}"/>
        </graphics>
      ''
      else "";
    vncServer = let
      cfg = devices.graphics.vnc;
    in
      if cfg.enable
      then ''
        <graphics type="vnc" port="${builtins.toString cfg.port}" listen="${cfg.listen}"/>
      ''
      else "";
    disks =
      mapSetToString (dev: {
        type,
        device,
        driver,
        source,
        bus,
        boot,
        extraConfig,
        ...
      }: let
        extension = lib.last (lib.splitString "." (builtins.unsafeDiscardStringContext source));
        diskType = diskTypes.${extension} or driver.type;
      in ''
        <disk type="${type}" device="${device}">
          <driver name="${driver.name}" type="${diskType}" ${
          if builtins.isNull driver.discard
          then ""
          else ''discard="${driver.discard}"''
        }/>
          <source file="${source}"/>
          <target dev="${dev}" bus="${bus}"/>
          ${
          if builtins.isNull boot
          then ""
          else ''<boot order="${builtins.toString boot}"/>''
        }
          ${extraConfig}
        </disk>
      '')
      devices.disks;
    sound = lib.concatStringsSep "\n" (builtins.map (model: ''<sound model="${model}"/>'') devices.sound);
    interfaces = mapSetToString (name: {
      type ? "network",
      macvtapType ? "bridge",
      mac ? null,
      model ? "virtio",
      extraConfig ? "",
    }: let
      source = {
        network = ''<source network="default"/>'';
        bridge = ''<source bridge="${name}"/>'';
        direct = ''<source dev="${name}" mode="${macvtapType}"/>'';
      };
      macField =
        if (isNull mac)
        then ""
        else ''<mac address="${mac}"/>'';
    in ''      <interface type="${type}">
        ${source.${type}}
        ${macField}
      	<model type="${model}"/>
        ${extraConfig}
      </interface>'')
    devices.network;
    video =
      if devices.video == "none"
      then ""
      else ''        <video>
        <model type="${devices.video}"/>
        </video>'';
  in
    pkgs.writeText "${name}.xml" ''
      <domain type='kvm'>
        <name>${name}</name>
        <uuid>${uuid}</uuid>
        <os>
          <type arch="${os.arch}" machine="${os.machine}">${os.type}</type>
          ${
        if builtins.isString os.uefi || os.uefi == true
        then ''
          <loader readonly="yes" type="pflash">${uefiLoader}</loader>
        ''
        else ""
      }
          <bootmenu enable="${mkYesNo os.bootmenu}"/>
          ${os.extraConfig}
        </os>
        <memory>${builtins.toString memory}</memory>
        <vcpu>${builtins.toString vcpu}</vcpu>
        <cpu mode="${cpu.mode}" >
          <topology sockets="${builtins.toString cpu.sockets}" cores="${builtins.toString cpu.cores}" threads="${builtins.toString cpu.threads}"/>
        </cpu>
        <features>
        ${
        if features.acpi
        then "<acpi/>"
        else ""
      }
        ${
        if features.apic
        then "<apic/>"
        else ""
      }
        <vmport state='${mkOnOff features.vmport}'/>
        ${features.extraConfig}
        </features>
        <devices>
          <console type="pty"/>
          ${video}
          ${spiceServer}
          ${vncServer}
          ${disks}
          ${sound}
          ${interfaces}
        ${devices.extraConfig}
        </devices>
        ${extraConfig}
        <redirdev bus="usb" type="spicevmc">
          <address type="usb" bus="0" port="1"/>
        </redirdev>
      </domain>
    '';
  mkNetwork = name: {
    uuid ? mkUUID name,
    extraConfig,
    ...
  }:
    pkgs.writeText "${name}.xml" ''
      <network>
         <name>${name}</name>
       <uuid>${uuid}</uuid>
       ${extraConfig}
       </network>
    '';
in {
  options.virtualisation.libvirtd = {
    domains = mkOptNamed domainOptions;
    networks = mkOptNamed networkOptions;
  };
  config = lib.mkIf config.tumble-config.vms.enable {
    systemd.services.setup-libvirt = {
      wantedBy = ["multi-user.target"];
      after = ["network.target"];
      script = ''

              # defined networks
              ${mapSetToString (
            name: networkConfig: let
              network = mkNetwork name networkConfig;
            in ''              # [${name}]
                    ${virsh} net-undefine ${name}|true
                    ${virsh} net-define ${network}|true
                    ${
                if networkConfig.autostart
                then ''
                  ${virsh} net-autostart ${name}|true
                  ${virsh} net-start ${name}|true
                ''
                else ""
              }
            ''
          )
          cfg.networks}

        # Define Virtual Macines
        ${mapSetToString (name: domainConfig: let
            domain = mkDomain name domainConfig;
          in ''            # [${name}]
                    mkdir -p /var/lib/libvirt/vms/${name}
                    cd /var/lib/libvirt/vms/${name}
                    ${mapSetToString (name: {
                source,
                size,
                driver,
                ...
              }: let
                extension = lib.last (lib.splitString "." (builtins.unsafeDiscardStringContext source));
                diskType = diskTypes.${extension} or driver.type;
              in ''
                if [ ! -e "${source}" ]; then
                  ${pkgs.qemu}/bin/qemu-img create -f ${diskType} "${source}" ${size}
                fi
              '')
              domainConfig.devices.disks}

            ${domainConfig.extraSetup}
            ${virsh} define ${domain}

          '')
          cfg.domains}'';
    };
  };
}
