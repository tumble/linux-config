{
  pkgs,
  lib,
  config,
  ...
}: let
  guestTools = pkgs.fetchurl {
    url = "https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso";
    hash = "sha256-vcKtFyegi22KWdQOES2TD1Ois1S974WQOrqtiWIU8KM=";
  };

  winRW = "/var/lib/libvirt/vms/win";
in {
  config = lib.mkIf config.tumble-config.vms.enable {
    environment.systemPackages = with pkgs; [
      win-virtio
      win-spice
    ];

    # virtualisation.libvirtd.hooks.win = {

    # };

    virtualisation.libvirtd.domains = {
      win = {
        cpu.cores = 6;
        os.uefi = true;
        features.acpi = true;
        features.apic = true;
        memory = 13671875;
        devices = {
          graphics.spice.enable = true;
          video = "virtio";
          disks = {
            sda = {
              source = "${winRW}/win.qcow2";
              size = "50G";
            };
            # sdb = {
            #   source = "${winRW}/Win10_21H1_English_x64.iso";
            #   device = "cdrom";
            #   bus = "sata";
            # };
            # sdc = {
            #   source = "${guestTools}";
            #   device = "cdrom";
            #   bus = "sata";
            # };
            # sdd.source = "${winRW}/win10.qcow2";
          };
          network.a = {};
        };
      };
      win-passthrough = {
        cpu.cores = 6;
        os.uefi = true;
        features.acpi = true;
        features.apic = true;
        memory = 13671875;
        devices = {
          video = "none";
          disks = {
            sda = {
              source = "${winRW}/win.qcow2";
              size = "50G";
            };
            sdb = {
              source = "${winRW}/Win10_21H1_English_x64.iso";
              device = "cdrom";
              bus = "sata";
            };
            sdc = {
              source = "${guestTools}";
              device = "cdrom";
              bus = "sata";
            };
            sdd.source = "${winRW}/win10.qcow2";
          };
          network.a = {};
          # pass through keybaord and mouse
          extraConfig = ''
            <hostdev mode="subsystem" type="usb" managed="yes">
              <source>
                <vendor id="0x046d"/>
                <product id="0xc31c"/>
              </source>
              <address type="usb" bus="0" port="1"/>
            </hostdev>
            <hostdev mode="subsystem" type="usb" managed="yes">
              <source>
                <vendor id="0x1bcf"/>
                <product id="0x0005"/>
              </source>
              <address type="usb" bus="0" port="2"/>
            </hostdev>
            <hostdev mode="subsystem" type="pci" managed="yes">
              <source>
                <address domain="0x0000" bus="0x03" slot="0x00" function="0x0"/>
              </source>
              <address type="pci" domain="0x0000" bus="0x06" slot="0x00" function="0x0"/>
            </hostdev>
            <hostdev mode="subsystem" type="pci" managed="yes">
              <source>
                <address domain="0x0000" bus="0x03" slot="0x00" function="0x1"/>
              </source>
              <address type="pci" domain="0x0000" bus="0x07" slot="0x00" function="0x0"/>
            </hostdev>
          '';
        };
      };
    };
  };
}
