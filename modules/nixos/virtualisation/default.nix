{
  lib,
  config,
  pkgs,
  ...
}: let
  imp = config.tumble-config.impermanence;
in {
  imports = [
    ./vmSetup.nix
    ./gpu.nix
    ./windows.nix
    ./mac.nix
  ];

  options.tumble-config.vms.enable = lib.mkEnableOption "Enable vms";

  config = lib.mkIf config.tumble-config.vms.enable {
    programs.dconf.enable = true;
    environment.systemPackages = with pkgs; [
      virt-manager
      virt-viewer
      spice
      spice-gtk
      spice-protocol
      win-virtio
      win-spice
      adwaita-icon-theme
      pciutils
      usbutils
      intel-gpu-tools
      #pkgs.OVMFFull
    ];
    networking.firewall.allowedTCPPorts = [16509];
    virtualisation = {
      libvirtd = {
        enable = true;
        networks.default = {
          extraConfig = ''
            <forward mode="nat">
              <nat>
                <port start="1024" end="65535"/>
              </nat>
            </forward>
            <bridge name="virbr0" stp="on" delay="0"/>
            <mac address="52:54:00:d8:2e:55"/>
            <ip address="192.168.122.1" netmask="255.255.255.0">
              <dhcp>
                <range start="192.168.122.2" end="192.168.122.254"/>
              </dhcp>
            </ip>
          '';
        };
        qemu = {
          #package = pkgs.qemu_kvm;
          swtpm.enable = true;
          ovmf.enable = true;
          #ovmf.packages = [pkgs.OVMFFull.fd];
        };
        extraConfig = ''
          log_filters="3:qemu 1:libvirt"
          log_outputs="2:file:/var/log/libvirt/libvirtd.log"
        '';
      };
      spiceUSBRedirection.enable = true;
    };
    services.spice-vdagentd.enable = true;

    environment.persistence.${imp.path} = lib.mkIf imp.enable {
      directories = [
        "/etc/libvirt/"
        "/var/lib/libvirt/images"
        "/var/lib/libvirt/storage"
        "/var/lib/libvirt/secrets"
        "/var/lib/libvirt/vms"
        "/etc/qemu/"

        "/var/lib/containers/"
        "/etc/containers/"
        "/var/lib/cni/"
      ];
    };
  };
}
