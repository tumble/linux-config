{
  pkgs,
  lib,
  config,
  ...
}: let
  macos = pkgs.fetchFromGitHub {
    owner = "foxlet";
    repo = "macOS-Simple-KVM";
    rev = "527588d5a25c232fa82bfa079cdb6771568f3d95";
    sha256 = "sha256-gQD4DWMdS/+U8Ekt5mQuDwzsKzLYCEPa773JfHgYodY=";
  };
  macosRW = "/var/lib/libvirt/vms/mac";
  getSystemImg = pkgs.writeShellApplication {
    name = "setup-macos";

    runtimeInputs = with pkgs; [bash python3 python312Packages.pip dmg2img];

    text = ''
      if [ ! -f  BaseSystem.img ]; then
      rm -rf venv FetchMacOS
      if [ ! -d FetchMacOS ]; then
      cp -r ${macos}/tools/FetchMacOS .
      fi
      if [ ! -d venv ]; then
        python3 -m venv venv
      fi
      if [ ! -f  BaseSystem.dmg ]; then
        # shellcheck disable=SC1091
        source ./venv/bin/activate
        pip install -r FetchMacOS/requirements.txt
        bash FetchMacOS/fetch.sh -v 10.15
        cp FetchMacOS/BaseSystem/BaseSystem.dmg .
        rm -rf FetchMacOS
      fi
        dmg2img BaseSystem.dmg BaseSystem.img

        rm -rf FetchMacOS venv

      fi
    '';
  };
in {
  config = lib.mkIf config.tumble-config.vms.enable {
    environment.systemPackages = with pkgs; [
      win-virtio
      win-spice
    ];

    virtualisation.libvirtd.domains.mac = {
      cpu.cores = 6;
      cpu.mode = "custom";
      os.uefi = "${macos}/firmware/OVMF_CODE.fd";
      os.machine = "q35";
      os.extraConfig = ''
        <nvram>${macosRW}/OVMF_VARS-1024x768.fd</nvram>
      '';
      features.acpi = true;
      features.apic = true;
      memory = 7812500;
      devices = {
        graphics.spice.enable = true;
        video = "qxl";
        disks = {
          sda.source = "${macosRW}/ESP.qcow2";
          sdb.source = "${macosRW}/BaseSystem.img";
          sdc = {
            source = "${macosRW}/HDD.qcow2";
            size = "50G";
          };
        };
      };
      extraConfig = ''
         <qemu:commandline>
          <qemu:arg value='-cpu'/>
          <qemu:arg value='Penryn,kvm=on,vendor=GenuineIntel,+invtsc,vmware-cpuid-freq=on,+pcid,+ssse3,+sse4.2,+popcnt,+avx,+aes,+xsave,+xsaveopt,check'/>
          <qemu:arg value='-device'/>
          <qemu:arg value='isa-applesmc,osk=ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc'/>
          <qemu:arg value='-smbios'/>
          <qemu:arg value='type=2'/>
        </qemu:commandline>
      '';

      extraSetup = ''
        cp ${macos}/firmware/OVMF_VARS-1024x768.fd .
        cp ${macos}/ESP.qcow2 .
        # ${getSystemImg}/bin/setup-macos

      '';
    };
  };
}
