{
  lib,
  config,
  pkgs,
  ...
}: let
  imp = config.tumble-config.impermanence;
in {
  options.tumble-config.vms.gpus = lib.mkOption {
    type = lib.types.listOf lib.types.str;
    default = [];
  };
  config = lib.mkIf config.tumble-config.vms.enable {
    boot = {
      # initrd.kernelModules = [
      #     "vfio_pci"
      #     "vfio"
      #     "vfio_iommu_type1"
      #     "vfio_virqfd"

      #     "nvidia"
      #     "nvidia_modeset"
      #     "nvidia_uvm"
      #     "nvidia_drm"
      # ];

      kernelParams = [
        "intel_iommu=on"
        ("vfio-pci.ids=" + lib.concatStringsSep "," config.tumble-config.vms.gpus)
      ];
    };

    virtualisation.libvirtd.hooks.qemu = {
      test = ./gpu-hook.sh;
    };
  };
}
