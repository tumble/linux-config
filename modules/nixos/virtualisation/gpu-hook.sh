#!/bin/sh
set -x

VM="$1"
OPERATION="$2"


if [ $VM == "win-passthrough" ]; then 

# echo test

case "$OPERATION" in
        	"prepare")
## vm start

# display manager
systemctl stop display-manager.service
# frame buffer
echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind
# vt consoles
echo 0 > /sys/class/vtconsole/vtcon0/bind
echo 0 > /sys/class/vtconsole/vtcon1/bind
# kernel modules
modprobe -r drm_kms_helper
/var/run/current-system/sw/bin/modprobe -r amdgpu
/var/run/current-system/sw/bin/modprobe -r radeon
modprobe -r drm

modprobe vfio
modprobe vfio_pci
modprobe vfio_iommu_type1

            ;;

            "release")

# vm end

# kernel modules
modprobe -r vfio_pci
modprobe -r vfio_iommu_type1
modprobe -r vfio


modprobe drm
/var/run/current-system/sw/bin/modprobe amdgpu
/var/run/current-system/sw/bin/modprobe radeon
modprobe drm_kms_helper
# vt consoles
echo 1 > /sys/class/vtconsole/vtcon0/bind
echo 1 > /sys/class/vtconsole/vtcon1/bind
# framebuffer
echo "efi-framebuffer.0" > /sys/bus/platform/drivers/efi-framebuffer/bind
# display manager
systemctl start display-manager.service



            ;;
	esac

sleep 30

fi