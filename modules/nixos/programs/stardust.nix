{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    stardust-xr-server
    flatland-xr
  ];
}
