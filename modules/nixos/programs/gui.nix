{
  pkgs,
  lib,
  config,
  ...
}: {
  config = lib.mkIf (config.tumble-config.desktop != "") {
    programs.firefox = {
      enable = true;
      package = pkgs.librewolf;
    };

    environment.systemPackages = with pkgs; [
      wl-clipboard
      qalculate-qt
      # evolution
      localsend
      gparted
      scrcpy
      #   feishin
      testdisk-qt
      shortwave
      fragments

      # video
      mpv
      # image
      xviewer
      feh
      # pdf
      mate.atril
      # music

      # mail
      geary

      # hex editor
      ghex

      pcmanfm-qt

      handbrake
    ];

    #programs.kdeconnect.enable = true;
  };
}
