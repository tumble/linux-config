{
  config,
  lib,
  pkgs,
  self,
  ...
}: {
  imports = [
    ./gui.nix
    ./kdeconnect.nix
    # ./stardust.nix
    # ./gnome-pim.nix
  ];

  config = {
    environment.systemPackages = with pkgs; [
      micro
      file
      killall
      samba
      gvfs
      rsync
      rclone
      mosh
      shared-mime-info
      lxmenu-data
      home-manager
      fzf

      yt-dlp
      micro
      tree
      neofetch
      htop
      # archives
      p7zip # .7z
      dar # .dar
      rar # .rar
      zip
      unzip # .zip
      lzip

      # scripts
      self.packages.${pkgs.system}.tumble-update
      (writeScriptBin "tumble-log" ''
        	set -e

        	pushd /etc/nixos>/dev/null
        	echo "=> Opening log"
        cat nixos-switch.log
        	popd>/dev/null
      '')
      (writeScriptBin "tumble-deptree" ''
        nix-store --query --tree /nix/store/$(ls -1 /nix/store | grep "$@")
      '')
      (writeScriptBin "create-ssh-key" ''
        set -e

        ask() {
        	eval "$1=\"$2\""
        	if [ -z "$2" ]; then
        		echo "$3"
        		read $1
        	fi
        }

        ask SSH_HOSTNAME "$1" "Enter hostname of remote server:"
        ask SSH_PORT "$2" "Enter ssh port of remote server:"
        ask SSH_PUBLIC "$3" "Enter public hostname of remote server:"

        ssh-keygen -t ed25519 -f $HOME/.ssh/$SSH_HOSTNAME

        cat << EOF
        ##################################
        SSH PUBLIC KEY:
        ##################################

        $(cat ~/.ssh/$SSH_HOSTNAME.pub)

        ###################################
        Put the following in .ssh/config:
        ###################################

        Host $SSH_HOSTNAME
        	#HostName $SSH_PUBLIC
        	Port $SSH_PORT
        	IdentityFile ~/.ssh/$SSH_HOSTNAME

        ####################################
        EOF
      '')
    ];

    programs.mosh.enable = true;
    services.gvfs.enable = true;
    # services.udisks2.enable = true;
    services.devmon.enable = true;
    users.defaultUserShell = pkgs.fish;
  };
}
