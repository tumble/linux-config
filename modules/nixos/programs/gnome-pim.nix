{
  pkgs,
  lib,
  config,
  ...
}: {
  config = lib.mkIf (config.tumble-config.desktop != "") {
    services.gnome.gnome-settings-daemon.enable = true;
    services.gnome.gnome-online-accounts.enable = true;

    environment.systemPackages = with pkgs; [
      gnome-calendar
      gnome-contacts
      gnome-clocks
      geary
      gnome-control-center
      gnome-online-accounts
    ];

    #programs.kdeconnect.enable = true;
  };
}
