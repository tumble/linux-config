{
  config,
  lib,
  self,
  ...
}: let
  localhost = config.networking.hostName;
  interface = config.tumble-config.interface;
  my = builtins.getAttr localhost self.hostsSet;
in {
  options.tumble-config = {
    home-lan.enable = lib.mkEnableOption "System is at home";
    interface = lib.mkOption {
      default = "eth0";
    };
  };
  config = lib.mkIf config.tumble-config.home-lan.enable {
    networking = {
      hosts =
        builtins.foldl' (acc: host:
          acc
          // {
            "${host.ip}" = [host.name host.fqdn];
          }) {
          "127.0.0.1" = [localhost "${localhost}.local" "localhost" "me"];
          "192.168.0.1" = ["homestead" "gateway" "trow-homestead" "homestead.local" "gateway.local" "trow-homestead.local"];
        }
        self.hostsList;

      interfaces.${interface} = {
        ipv4.addresses = [
          {
            address = my.ip;
            prefixLength = 24;
          }
        ];
      };

      defaultGateway = {
        address = "192.168.0.1";
        inherit interface;
      };
      nameservers = ["9.9.9.9" "149.112.112.112" "2620:fe::fe" "2620:fe::9"];

      firewall = {
        allowedTCPPorts = [137 138 139];
        allowedUDPPorts = [137 138 139];
      };
    };
  };
}
