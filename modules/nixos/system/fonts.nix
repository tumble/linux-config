{pkgs, ...}: {
  fonts = {
    enableDefaultPackages = true;
    packages = with pkgs; [
      vazir-fonts
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
      liberation_ttf
      fira-code
      fira-code-symbols
      mplus-outline-fonts.githubRelease
      dina-font
      proggyfonts
      texlivePackages.oswald
      ubuntu-sans
      ubuntu-sans-mono
      roboto
      winePackages.fonts
      vistafonts
    ];

    fontDir.enable = true;

    fontconfig = {
      #   defaultFonts = {
      #     serif = ["Liberation Serif" "Vazirmatn"];
      #     sansSerif = ["Ubuntu" "Vazirmatn"];
      #     monospace = ["Ubuntu Mono"];
      #   };
      useEmbeddedBitmaps = true;
    };
  };
}
