{...}: {
  imports = [
    ./boot.nix
    ./home-manager.nix
    ./impermenance.nix
    ./nixpkgs.nix
    ./lan.nix
    ./fonts.nix
    ./sops.nix
  ];
}
