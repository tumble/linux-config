{
  config,
  inputs,
  lib,
  pkgs,
  self,
  ...
}: {
  options.tumble-config.updates.enable = lib.mkEnableOption "Enable Autoupdate";
  config = {
    nix = {
      package = pkgs.lix;
      settings.experimental-features = ["nix-command" "flakes"];
      #channel.enable = false;
      gc = {
        automatic = true;
        dates = "weekly";
      };
    };

    nixpkgs.config = {
      allowUnfree = true;
      # allowInsecure = true;
    };

    nixpkgs.overlays = [inputs.tumblepkgs.overlays.default self.overlays.default];

    system.autoUpgrade = lib.mkIf config.tumble-config.updates.enable {
      enable = true;
      flake = "git+https://git.disroot.org/tumble/linux-config?ref=nixos";
      dates = "daily";
      persistent = true;
      randomizedDelaySec = "45min";
      # kernel updates
      allowReboot = true;
      rebootWindow = {
        lower = "03:00";
        upper = "07:00";
      };
    };

    # Copy the NixOS configuration file and link it from the resulting system
    # (/run/current-system/configuration.nix). This is useful in case you
    # accidentally delete configuration.nix.
    # system.copySystemConfiguration = true;

    # This option defines the first version of NixOS you have installed on this particular machine,
    # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
    #
    # Most users should NEVER change this value after the initial install, for any reason,
    # even if you've upgraded your system to a new NixOS release.
    #
    # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
    # so changing it will NOT upgrade your system.
    #
    # This value being lower than the current NixOS release does NOT mean your system is
    # out of date, out of support, or vulnerable.
    #
    # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
    # and migrated your data accordingly.
    #
    # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
    system.stateVersion = lib.mkDefault (builtins.throw ''Please add system.stateVersion = "${config.system.nixos.release}"; to the nixos config'');
  };
}
