{
  inputs,
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.tumble-config.impermanence;
in {
  imports = [
    inputs.impermanence.nixosModules.impermanence
  ];

  options.tumble-config.impermanence = {
    enable = lib.mkEnableOption "Enable Impermanence";
    path = lib.mkOption {
      type = lib.types.str;
      default = "/storage/root";
    };
    persistDefault = lib.mkEnableOption "Persust the normal files";
    files = lib.mkOption {
      default = [];
      type = lib.types.listOf lib.types.raw;
    };
    directories = lib.mkOption {
      default = [];
      type = lib.types.listOf lib.types.raw;
    };
    home = {
      enable = lib.mkEnableOption "Enable HM Impermaence";
      path = lib.mkOption {
        type = lib.types.str;
        default = "/storage/home";
      };
      files = lib.mkOption {
        default = [];
        type = lib.types.listOf lib.types.raw;
      };
      directories = lib.mkOption {
        default = [];
        type = lib.types.listOf lib.types.raw;
      };
    };
  };
  config = let
    sysConfig = lib.mkIf cfg.enable {
      tumble-config.impermanence.home.enable = lib.mkDefault true;
      tumble-config.impermanence.persistDefault = lib.mkDefault true;
      fileSystems = {
        ${cfg.path}.neededForBoot = true;
        # todo seperate into an if Cfs.home.enable
        #${cfg.home.path}.neededForBoot = cfg.home.enable;
      };
      environment.persistence.${cfg.path} = {
        hideMounts = true;
        directories = lib.mkMerge [
          (lib.mkIf cfg.persistDefault [
            "/etc/nixos"
            "/var/log"
            "/var/lib/nixos"
            "/var/lib/systemd/coredump"
          ])
          cfg.directories
        ];
        files = lib.mkMerge [
          #   (lib.mkIf cfg.persistDefault [
          #     "/etc/machine-id"
          #   ])
          []
          cfg.files
        ];
      };
      programs.fuse.userAllowOther = true;
      system.activationScripts = {
        etc_shadow = ''
          [ -f "/etc/shadow" ] && cp /etc/shadow ${cfg.path}/etc/shadow
          [ -f "${cfg.path}/etc/shadow" ] && cp ${cfg.path}/etc/shadow /etc/shadow
        '';

        users.deps = ["etc_shadow"];
      };
    };
    homeConfig = lib.mkIf cfg.home.enable {
      fileSystems.${cfg.home.path}.neededForBoot = true;
    };
  in
    lib.mkMerge [sysConfig homeConfig];
}
