{
  inputs,
  lib,
  self,
  config,
  ...
}:
with builtins;
let
hmConfigRoot = ../../../home;
  hasHMConfig =name: pathExists (hmConfigRoot + "/${name}") || pathExists (hmConfigRoot + "/${name}.nix");
 getHome = name: 
 if (pathExists (hmConfigRoot + "/${name}")) then (hmConfigRoot + "/${name}") else
 (if (pathExists (hmConfigRoot + "/${name}.nix")) then (hmConfigRoot + "/${name}.nix") else "");
  users = config.users.users;
  includeUsers =
   	filter
    (username: users.${username}.isNormalUser && (hasHMConfig username)) (attrNames users);
  homes =
    builtins.map (
      name: {
        inherit name;
        value = import (getHome name);
      }
    )
    includeUsers;
in {
  imports = [
    inputs.home-manager.nixosModules.default
  ];
  home-manager = {
    sharedModules = [self.homeManagerModule];
    extraSpecialArgs = {inherit self inputs;};
    users = listToAttrs homes;
    useGlobalPkgs = true;
    #useUserPackages = true;
    backupFileExtension = "backup-8";
    verbose = true;
  };
}
