{
  config,
  lib,
  ...
}: {
  options.tumble-config.headless = lib.mkEnableOption "If screen will not be used often";

  # Use the systemd-boot EFI boot loader.
  config.boot = {
    loader = {
      systemd-boot = {
        enable = true;
        configurationLimit = 5;
        consoleMode = "max";
      };
      efi.canTouchEfiVariables = true;

      #tempting
      #kernelPackages = pkgs.linuxPackages_zen;
    };
    supportedFilesystems = ["ntfs"];

    loader.timeout = lib.mkIf (!config.tumble-config.headless) (lib.mkDefault 0);
    initrd.verbose = config.tumble-config.headless;
    plymouth.enable = !config.tumble-config.headless;
    kernelParams = lib.mkIf (!config.tumble-config.headless) [
      "quiet"
      "splash"
      "boot.shell_on_fail"
      "loglevel=3"
      "efi=keep"
      "rd.systemd.show_status=false"
      "rd.udev.log_level=3"
      "udev.log_priority=3"
    ];
  };
}
