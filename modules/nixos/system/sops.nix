{
  config,
  lib,
  inputs,
  ...
}: {
  imports = [inputs.sops-nix.nixosModules.sops];

  options.tumble-config.sops.enable = lib.mkEnableOption "Enable Sops";
  options.tumble-config.sops.format = lib.mkOption {
    type = lib.types.str;
    description = "secrets format";
    default = "json";
  };
  config = lib.mkIf config.tumble-config.sops.enable {
    sops = {
      defaultSopsFile = ../../../secrets + "/${config.networking.hostName}.${config.tumble-config.sops.format}";
      defaultSopsFormat = config.tumble-config.sops.format;
      age = {
        generateKey = true;
        sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];
      };
    };
  };
}
