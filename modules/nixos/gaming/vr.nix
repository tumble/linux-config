{
  lib,
  config,
  pkgs,
  self,
  ...
}: {
  options.tumble-config.gaming.vr.enable = lib.mkEnableOption "Enable VR";

  config = lib.mkIf config.tumble-config.gaming.vr.enable {
    #boot.kernelPatches = [
    #	{
    #		name = "amdgpu-ignore-ctx-privileges";
    #		patch = pkgs.fetchpatch {
    #			name = "cap_sys_nice_begone.patch";
    #			url = "https://github.com/Frogging-Family/community-patches/raw/master/linux61-tkg/cap_sys_nice_begone.mypatch";
    #			hash = "sha256-Y3a0+x2xvHsfLax/uwycdJf3xLxvVfkfDVqjkxNaYEo=";
    #		};
    #	}
    #];

    services.monado = {
      enable = true;
      defaultRuntime = true; # Register as default OpenXR runtime
      #   package = pkgs.monado.overrideAttrs (prevAttrs: {
      #     patches = prevAttrs.patches or [] ++ [
      #         ./now_ns_and_no_dropped_frames.patch
      #     ];
      #   });
    };

    programs.corectrl.enable = true;
    systemd.user.services.monado = {
      enable = true;
      environment = {
        STEAMVR_LH_ENABLE = "1";
        XRT_COMPOSITOR_COMPUTE = "1";
        U_PACING_COMP_MIN_TIME_MS = "5";
      };
    };
    services.xserver.enable = lib.mkDefault true;

    # services.xserver.desktopManager = {
    #   xterm.enable = false;
    #   xfce.enable = true;
    # };

    environment.systemPackages = with pkgs; [
      #qt6.qtwayland
      #pkgsi686Linux.gperftools
      # opencomposite-helper
      glxinfo
      #   BeatSaberModManager
      wlx-overlay-s
    ];
  };
}
