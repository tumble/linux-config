{
  lib,
  config,
  pkgs,
  ...
}: {
  options.tumble-config.gaming.streaming.enable = lib.mkEnableOption "Enable Game Streaming";

  config = lib.mkIf config.tumble-config.gaming.streaming.enable {
    services.sunshine = {
      enable = true;
      autoStart = true;
      capSysAdmin = true;
      openFirewall = true;
    };
  };
}
