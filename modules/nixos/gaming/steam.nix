{
  config,
  lib,
  pkgs,
  ...
}: {
  config = lib.mkIf config.tumble-config.gaming.enable {
    programs.steam = {
      enable = true;
      remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
      dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
      localNetworkGameTransfers.openFirewall = true;
      extest.enable = true;
    };
    nixpkgs.config.allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) [
        "steam"
        "steam-original"
        "steam-run"
      ];
    #services.xserver.enable = lib.mkDefault true;
    programs.gamemode.enable = true;
    programs.steam.gamescopeSession.enable = lib.mkDefault false;

    environment.systemPackages = with pkgs; [
      mangohud
      protonup-qt
      protontricks
    ];
  };
}
