{
  self,
  lib,
  pkgs,
  config,
  ...
}: {
  imports = [
    ./steam.nix
    ./waydroid.nix
    ./vr.nix
    ./sunshine.nix
    ./capture-card.nix
    ./emulation.nix
    ./steam-presense.nix
  ];

  options.tumble-config.gaming.enable = lib.mkEnableOption "Enable Gaming";
  config = lib.mkIf config.tumble-config.gaming.enable {
    #JoyCon
    services.joycond.enable = true;
    #Gamcube
    services.udev.packages = [pkgs.dolphin-emu];
    boot.extraModulePackages = [
      config.boot.kernelPackages.gcadapter-oc-kmod
    ];
    environment.systemPackages = with pkgs; [
      goverlay

      # wine
      wineWowPackages.staging
      wineWowPackages.waylandFull
      winetricks
      icoextract

      #darling
      # darling
      # darling-dmg

      # games
      itch
      #   heroic
      chiaki
      prismlauncher
      minetest
      itch
      gnome-mines
      superTuxKart
      superTux
      xonotic
      aisleriot
      yargNightly
    ];

    #programs.java = { enable = true; };
  };
}
