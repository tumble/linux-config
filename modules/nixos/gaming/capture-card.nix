{
  self,
  lib,
  pkgs,
  config,
  ...
}: let
  selfPkgs = self.packages.${pkgs.system};
in {
  options.tumble-config.gaming.capture-card.enable = lib.mkEnableOption "Enable Elgato Game Capture HD";
  config = lib.mkIf config.tumble-config.gaming.capture-card.enable {
    environment.systemPackages = with selfPkgs; [
      (pkgs.writeScriptBin "gchd" ''
        cd ${selfPkgs.elgato-gchd-firmware}/lib/firmware/gchd
        ${selfPkgs.elgato-gchd}/bin/gchd $@ &
        GCHD_PID=$!
        sleep 1
        #mpv /tmp/gchd.ts &
        wait $GCHD_PID
      '')
    ];
    services.udev.packages = with selfPkgs; [selfPkgs.elgato-gchd-udev];
  };
}
