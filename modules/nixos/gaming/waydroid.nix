# Useful waydroid stuff:
# GAPPS, Magisk, ARM: https://github.com/casualsnek/waydroid_script
# android tv: https://github.com/supechicken/waydroid-androidtv-build/blob/main/BUILDING.md
{
  lib,
  config,
  self,
  pkgs,
  ...
}: let
  imp = config.tumble-config.impermanence or {};

  android-tv = let
    atv-image = pkgs.stdenv.mkDerivation {
      name = "atv-image";
      src = pkgs.fetchurl {
        url = "https://github.com/supechicken/waydroid-androidtv-build/releases/download/20241215/lineage-20.0-20241215-UNOFFICIAL-SupeChicken666-WaydroidATV.zip";
        hash = "sha256-BaBIwyflHTsMi9a7n6s5RAyffoXu6ih1VVQbwTGqflk=";
      };
      buildInputs = [pkgs.unzip];
      unpackPhase = ''
        unzip $src
      '';
      installPhase = ''
        mkdir -p $out
        cp *.img $out
      '';
    };
    atv-scripts = pkgs.fetchgit {
      url = "https://github.com/Waydroid-ATV/androidtv_scripts";
      rev = "bbca8e1c637acb6e7550381f6a67b0e0f1faa773";
      hash = "sha256-lU7guAxQScGbv3eaxfgiiHttM3BoD+n0n0OESfRf3i4=";
    };
  in
    pkgs.writeScriptBin "android-tv" ''
      IMGS=$(mktemp -d)
      mkdir -p $IMGS
      cp  ${atv-image}/* $IMGS
      waydroid init -f -i $IMGS
      cd ${atv-scripts}
      sh ./install-widevine-a13.sh
      sh ./install-libhoudini-a13.sh
      sh ./install-mindthegapps.sh
    '';
in {
  config = lib.mkIf config.tumble-config.gaming.enable {
    virtualisation.waydroid.enable = true;
    environment.persistence.${imp.path}.directories = lib.mkIf imp.enable ["/var/lib/waydroid"];

    environment.systemPackages = [
      self.packages.${pkgs.system}.waydroid-tools
      android-tv
      pkgs.wl-clipboard
    ];
  };
}
