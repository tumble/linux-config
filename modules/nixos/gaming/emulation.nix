{
  self,
  lib,
  pkgs,
  config,
  ...
}: {
  options.tumble-config.gaming.emulation.enable = lib.mkEnableOption "Enable Emulation";
  config = lib.mkIf config.tumble-config.gaming.emulation.enable {
    # fileSystems."/home/tumble/archive" = {
    # 	device = "tumble@onyx";
    # 	fsType = "sshfs";
    # 	neededForBoot = false;
    # 	options = [
    # 		"user"
    # 		"nodev"
    # 		"noatime"
    # 		"allow_other"
    # 		"_netdev"
    # 		"reconnect"
    # 		# "IdentityFile=/home/tumble/.ssh/onyx"
    # 	];
    # };

    environment.systemPackages = with pkgs; [
      fceux
      #   snes9x-gtk
      rmg-wayland
      dolphin-emu
      cemu

      sameboy
      mgba
      melonDS
      #lime3ds

      duckstation
      rpcs3
      ppsspp-qt

      dosbox
      _86Box-with-roms

      xemu
      retroarch
      #   (retroarchFull.override {
      #     cores = with libretro; [
      #       nestopia
      #       bsnes-hd
      #       mupen64plus
      #       parallel-n64
      #       dolphin

      #       gambatte
      #       mgba
      #       melonds
      #       citra

      #       ppsspp
      #       swanstation
      #       pcsx2
      #     ];
      #   })
    ];
  };
}
