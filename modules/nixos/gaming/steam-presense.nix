{
  pkgs,
  lib,
  config,
  ...
}: let
  repo = pkgs.fetchFromGitHub {
    owner = "JustTemmie";
    repo = "steam-presence";
    rev = "ffd8054ff6ede09e229d19e5c4b23bd117a9885c";
    sha256 = "sha256-fR655Kvww8xN/lo5MwsKrM0yw4bFyIcb59sSQa5WlJc=";
  };
  dir = "$HOME/.local/share/steam-presense";
in {
  options.tumble-config.gaming.steam-presence = lib.mkEnableOption "Enable Steam rich presence";
  config = lib.mkIf config.tumble-config.gaming.steam-presence {
    sops.secrets = {
      steam-presence = {
        format = "yaml";
        mode = "0440";
        owner = "tumble";
        group = "users";
      };
    };

    systemd.user.services.steam-presence = {
      enable = true;
      description = "Discord rich presence from Steam on Linux";
      documentation = ["https://github.com/JustTemmie/steam-presence"];
      wantedBy = ["default.target"];
      script = let
        python = "${pkgs.python39}/bin/python3";
      in ''
        whoami
        if [ ! -d "${dir}" ]; then
        mkdir -p "${dir}"
        echo "created ${dir}"
        fi
        cd "${dir}"

        if [ ! -d src ]; then
        mkdir -p src
        rm -rf src/*|true
        echo "copying steam-presence source"
        cp -r ${repo}/* src
        fi

        if [ ! -d venv ]; then
        echo "creating python venv"
        ${python} -m venv venv
        fi

        rm config.json|true
        echo "importing config"
        cp ${config.sops.secrets.steam-presence.path} config.json

        source venv/bin/activate
        pip install -r src/requirements.txt
        python3 src/main.py
      '';
      serviceConfig = {
        # This tells Systemd that steam-presence is a simple service:
        # When launched, the Python process stays running until the service exits.
        Type = "simple";

        # This tells the OS to treat steam-presence as a lowest-priority program.
        Nice = 19;

        # When steam-presence is told to exit, it throws a KeyboardInterrupt Python
        # exception.  This tells Systemd that it's OK, and that should count as a
        # normal program exit.
        SuccessExitStatus = 130;

        # This locks down what steam-presence is able to do.  It is not able to
        # get any more privileges than it already has, and almost the entire
        # filesystem is made read-only to it.  The only thing made read/write is
        # the directory where steam-presence lives.  That is so the icons.txt file
        # can be updated with new game icons (only when you use SteamGridDB).
        NoNewPrivileges = true;
        ProtectSystem = "strict";
      };
    };
  };
}
