{
  lib,
  config,
  pkgs,
  inputs,
  ...
}: {
  config = lib.mkIf (config.tumble-config.desktop == "hyprland") {
    services.xserver.enable = true;
    services.displayManager = {
        sddm = {
          enable = !config.tumble-config.htpc.enable;
          wayland.enable = !config.tumble-config.htpc.enable;
        };
      };

    nix.settings = {
        substituters = ["https://hyprland.cachix.org"];
        trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
    };

    programs.hyprland = with inputs.hyprland.packages.${pkgs.system};{
      enable = true;
      xwayland.enable = true;
      withUWSM = true;
      #package = hyprland;
      #portalPackage = xdg-desktop-portal-hyprland;
    };

    environment.sessionVariables = {
      WLR_NO_HARDWARE_CURSORS = "1";
      NIXOS_OZONE_WL = "1";
    };

    hardware.graphics.enable = true;
  };
}
