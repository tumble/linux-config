{
  lib,
  config,
  pkgs,
  ...
}: let
  login = config.tumble-config.login or "";
in {
  imports = [
    ./cosmic.nix
    ./hyprland.nix
    ./gnome.nix
    ./plasma.nix
    ./sway.nix
  ];

  options.tumble-config = {
    desktop = lib.mkOption {
      type = lib.types.str;
      description = "desktop choice";
      default = "";
    };
  };

  config = lib.mkIf (config.tumble-config.desktop != "") {
    environment.systemPackages = with pkgs; [
      slurp
      grim
      wl-clipboard
    ];
  };
}
