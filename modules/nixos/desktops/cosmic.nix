{
  config,
  lib,
  inputs,
  ...
}: let
  cosmic = inputs.nixos-cosmic.nixosModules;
in {
  imports = [
    {
      nix.settings = {
        substituters = ["https://cosmic.cachix.org/"];
        trusted-public-keys = ["cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE="];
      };
    }
    cosmic.default
  ];

  config = lib.mkIf (config.tumble-config.desktop == "cosmic") {
    services.desktopManager.cosmic.enable = true;
    services.displayManager.cosmic-greeter.enable = !config.tumble-config.htpc.enable;
  };
}
