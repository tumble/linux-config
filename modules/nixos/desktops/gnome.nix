{
  pkgs,
  lib,
  config,
  ...
}: {
  config = lib.mkIf (config.tumble-config.desktop == "gnome") {
    services.xserver.enable = lib.mkForce true;
    services.xserver.displayManager.gdm.enable = !config.tumble-config.htpc.enable;
    services.xserver.desktopManager.gnome.enable = true;

    programs.kdeconnect.package = lib.mkForce pkgs.gnomeExtensions.gsconnect;

    # services.accounts-daemon.enable = true;
    # services.gnome.gnome-settings-daemon.enable = true;
    # services.gnome.gnome-online-accounts.enable = true;
    # programs.dconf.enable = true;
    services.gnome.gnome-keyring.enable = true;

    #environment.gnome.excludePackages = with pkgs;
    #	[
    #		gnome-photos
    #		gedit
    #	]
    #	++ (with pkgs.gnome; [
    #		gnome-music
    #		gnome-terminal
    #		geary
    #		evince
    #		gnome-characters
    #		totem
    #		tali
    #		iagno
    #		hitori
    #		atomix
    #	]);

    # environment.systemPackages = with pkgs; [
    #   gnome.gvfs
    #   gvfs
    #   gnome-online-accounts
    #   adwaita-icon-theme
    #   gnome-tweaks
    #   gnome-settings-daemon
    #   gnomeExtensions.appindicator
    #   gnomeExtensions.sound-output-device-chooser
    #   gnomeExtensions.clipboard-indicator
    # ];

    # environment.sessionVariables = {
    # #   WLR_NO_HARDWARE_CURSORS = "1";
    # #   NIXOS_OZONE_WL = "1";
    # #   WEBKIT_FORCE_SANDBOX = "0";
    # };

    # qt = {
    #   enable = true;
    #   platformTheme = "gnome";
    #   style = "adwaita-dark";
    # };
  };
}
