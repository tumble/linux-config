{
  config,
  lib,
  inputs,
  ...
}: {

  config = lib.mkIf (config.tumble-config.desktop == "sway") {
    security.polkit.enable = true;

    services.xserver.enable = true;
	services.displayManager = {
        sddm = {
         enable = !config.tumble-config.htpc.enable;
        wayland.enable = !config.tumble-config.htpc.enable;
        };
      };


    programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true;
  };
  };
}
