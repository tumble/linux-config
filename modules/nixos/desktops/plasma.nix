{
  pkgs,
  lib,
  config,
  ...
}: {
  config = lib.mkIf (config.tumble-config.desktop == "plasma") {
    services = {
      xserver.enable = true;
      displayManager.sddm.enable = !config.tumble-config.htpc.enable;
      desktopManager.plasma6.enable = true;
    };

    programs = {
      chromium.enablePlasmaBrowserIntegration = true;
      kde-pim = {
        enable = true;
        #calendar
        merkuro = true;
        #contacts
        kontact = true;
        #mail
        kmail = true;
      };
    };

    environment.plasma6.excludePackages = with pkgs.kdePackages; [
      oxygen
      #   okular
      #   gwenview
      kmail
      #   ark
      elisa
      pkgs.xfce.parole
    ];

    environment.systemPackages = with pkgs.kdePackages; [
      kio-fuse
      kio-extras
      pim-data-exporter
      qtsvg
      qtwayland
      krfb
      kdegraphics-thumbnailers
      kimageformats
      merkuro
      kasts
    ];
  };
}
