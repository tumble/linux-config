{lib, ...}: {
  imports = [
    ./pipewire.nix
    ./lang.nix
    ./bluetooth.nix
    ./network.nix
  ];
}
