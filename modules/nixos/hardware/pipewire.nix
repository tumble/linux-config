{
  lib,
  pkgs,
  config,
  ...
}: {
  security.rtkit.enable = true;
  services.pulseaudio.enable = false;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };
  environment.systemPackages = with pkgs;
    lib.mkIf (config.tumble-config.desktop != "") [
      pavucontrol # volume control
      qpwgraph # patchbay
    ];
}
