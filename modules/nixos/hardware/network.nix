{
  config,
  lib,
  ...
}: let
  imp = config.tumble-config.impermanence;
in {
  options.tumble-config.wifi = lib.mkEnableOption "has wifi";
  options.tumble-config.nm = lib.mkEnableOption "enable network manager";
  config = {
    networking = {
      wireless.iwd.enable = config.tumble-config.wifi;
      networkmanager.enable = config.tumble-config.nm;
    };

    environment.persistence.${imp.path}.directories = lib.mkIf imp.enable (lib.mkIf config.tumble-config.nm [
      "/etc/NetworkManager/system-connections"
    ]);
  };
}
