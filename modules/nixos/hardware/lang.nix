{
  pkgs,
  lib,
  ...
}: let
  us = "en_US.UTF-8";
  gb = "en_GB.UTF-8";
  japan = "ja_JP.UTF-8";

  setLocal = local: {
    LANGUAGE = local;
    LC_ALL = local;
    LC_ADDRESS = local;
    LC_IDENTIFICATION = local;
    LC_MEASUREMENT = local;
    LC_MONETARY = local;
    LC_NAME = local;
    LC_NUMERIC = local;
    LC_PAPER = local;
    LC_TELEPHONE = local;
    LC_TIME = local;
  };
in {
  services.xserver.xkb.layout = "gb";
  time.timeZone = "Europe/London";
  i18n = {
    defaultLocale = "en_GB.UTF-8";
    supportedLocales = [
      "en_US.UTF-8/UTF-8"
      "en_GB.UTF-8/UTF-8"
      "ja_JP.UTF-8/UTF-8"
    ];
  };

  console = {
    #font = "La2-Terminus16";
    keyMap = lib.mkDefault "uk";
    useXkbConfig = true; # use xkb.options in tty.
  };

  i18n.extraLocaleSettings = setLocal gb;

  i18n.inputMethod = {
    enable = true;
    type = "fcitx5";
  };
  i18n.inputMethod.fcitx5.addons = with pkgs; [fcitx5-mozc fcitx5-table-extra];
}
