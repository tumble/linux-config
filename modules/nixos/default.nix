{...}: {
  imports = [
    ./gaming
    ./hardware
    ./programs
    ./system
    ./desktops
    ./projects
    ./virtualisation
    ./servers
    ./htpc
  ];
}
