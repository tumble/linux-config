{
  self,
  inputs,
  config,
  lib,
  pkgs,
  ...
}: {
  # VERSION AT INSTALL
  system.stateVersion = "23.11";

  imports = [
    inputs.disko.nixosModules.default
    (import ./disko.nix {devices = ["/dev/disk/by-id/wwn-0x5001b444a97c483c" "/dev/disk/by-id/wwn-0x500a0751e6948ec0"];})
    ./hardware-configuration.nix
  ];

  tumble-config = {
    desktop = "plasma";
    impermanence.enable = true;
    impermanence.home.enable = false;
    home-lan.enable = true;
    interface = "enp0s31f6";
    nm = false;
    updates.enable = true;
    sops = {
      enable = true;
      format = "yaml";
    };
    projects = {
      enable = true;
      unity.enable = false;
      obs.enable = true;
    };
    gaming = {
      enable = true;
      vr.enable = true;
      streaming.enable = false;
      capture-card.enable = true;
      emulation.enable = true;
      steam-presence = false;
    };
    vms = {
      enable = true;
      gpus = ["1002:73ff" "1002:ab28"];
    };
    # servers.pihole.enable = true;
    servers.ssh.enable = true;
  };

  official.systemBase = {
    enable = true;
    owner = "Tumble";
  };

  programs.fish.enable = true;
  programs.fish.promptInit = ''
    ${pkgs.any-nix-shell}/bin/any-nix-shell fish --info-right | source
  '';

  # Enable touchpad support (enabled default in most desktopManager).
  #services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.mutableUsers = false;
  users.users.tumble = {
    isNormalUser = true;
    extraGroups = ["wheel" "input" "kvm" "disk" "libvirtd" "audio" "video" "networkmanager"];
    hashedPassword = "$y$j9T$wrqA.yNCCkrlVCBFioz/l.$bAghMraXXKDoe4RAcmp.aJSEGDptyr0YbTCzvNHtzA4";
  };

  networking = let
    interface = "enp0s31f6";
  in {
    hostName = "latias"; # Define your hostname.
    firewall = {
      enable = true;
      allowedTCPPorts = [7770 7777 7771];
      allowedUDPPorts = [7770 7777 7771];
      interfaces.${interface} = {
        allowedTCPPorts = [7770 7771];
        allowedUDPPorts = [7770 7771];
      };
    };

    # interfaces.${interface} = {
    # 	ipv4.addresses = [
    # 		{
    # 			address = "192.168.0.8";
    # 			prefixLength = 24;
    # 		}
    # 	];
    # };
    # defaultGateway = {
    # 	address = "192.168.0.1";
    # 	interface = "enp0s31f6";
    # };
    # nameservers = ["9.9.9.9"];
  };

  nixpkgs.config.permittedInsecurePackages = [
    "olm-3.2.16"
  ];

  environment.systemPackages = with pkgs; [
    spotify
    # discord
    discord-canary
    nheko
    hunspell
    hunspellDicts.en_GB-large

    kodi

    gwc
  ];

  programs.steam.gamescopeSession.enable = true;

  services.udev.packages = [
    self.packages.${pkgs.system}.mudkip-wechip
  ];
}
