{
  config,
  pkgs,
  inputs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
    #./ficial/system-base.nix
    inputs.ficial-config.outPath
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  time.timeZone = "Europe/London";
  i18n.defaultLocale = "en_GB.UTF-8";

  networking = {
    hostName = "alakazam";
    hostId = "dab14ab3b";
    defaultGateway = {
      address = "192.168.0.1";
      interface = "wlan0";
    };
    nameservers = ["9.9.9.9" "149.112.112.112" "2620:fe::fe" "2620:fe::9"];
    firewall.allowedTCPPorts = [80 443];
    firewall.allowedUDPPorts = [];
  };

  tumble-config = {
    updates.enable = true;
    headless = true;
    wifi = true;
    servers.ssh.enable = true;
    home-lan.enable = true;
    interface = "wlan0";
  };

  official.systemBase = {
    enable = true;
    owner = "Tumble";
    #sshPorts = [3024];
    #moshPorts = {
    #	from = 60500;
    #	to = 60600;
    #};
  };

  services.caddy = {
    enable = true;
    globalConfig = ''
      auto_https off
    '';
    extraConfig = ''
      (common) {
      	encode zstd gzip
      	#log
      	#handle_errors {}
      	#rate_limit {}
      }
      http://tumble.ficial.net {
      	import common
      	root * /var/www/default
      	file_server
      }
    '';
  };
  #security.acme = {
  #  acceptTerms = true;
  #  defaults.email = "foo@bar.com";
  #};

  #systemd.services.bean = {
  #	wantedBy = ["multi-user.target"];

  #	serviceConfig = {
  #		Type = "oneshot";
  #		ExecStart = "${pkgs.git}/bin/git init /var/www/bean";
  #		#ExecStart = ''
  #		#	echo=${pkgs.coreutils}/bin/echo
  #		#	git=${pkgs.coreutils}/bin/git
  #		#	chmod=${pkgs.coreutils}/bin/chmod
  #		#
  #		#	${pkgs.coreutils}/bin/mkdir -p /var/www/bean
  #		#	$echo "hello">/var/www/bean/hi.txt

  #		#	dir=/var/www/bean
  #		#	if [ ! -d $dir/.git ]; then
  #		#		$git init $dir
  #		#	fi
  #		#	hook=$dir/.git/hooks/post-receive
  #		#	if [ ! -f $hook ]; then
  #		#		$echo "#!/bin/sh" > $hook
  #		#		$echo "$git --work-tree=$dir reset --hard" >> $hook
  #		#		$chmod +x $hook
  #		#	fi
  #		#'';
  #	};
  #};

  # TODO user disk quota

  # don't forget to set user authorized ssh keys
  # and don't put them in the config! put it in their home folders so they can change it later
  # user passwords default to locked, which means they can ssh in but no password works
  # expiring a password makes it impossible to log in through ssh because
  # when it tries to prompt for a password change it can't find passwd
  # for now just set a temporary password for new users and tell them to change it
  # in the future system-base.nix should make new users set a password on first ssh in
  users.users = {
    sarpnt = {
      isNormalUser = true;
      description = "SArpnt";
      extraGroups = ["wheel"];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA/trguK8JO48Bxv8YCiPW+IqRSa50DqjnsklV3C2foI sarpnt@ace"
      ];
    };
    tumble = {
      isNormalUser = true;
      description = "Tumble";
      extraGroups = ["wheel"];
      hashedPassword = "$y$j9T$wrqA.yNCCkrlVCBFioz/l.$bAghMraXXKDoe4RAcmp.aJSEGDptyr0YbTCzvNHtzA4";

      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPgUYyOe74nzSji3Yfrrw7S940VoHUdybbxFsdlVC1Xr tumble@tumble-pc"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEWjcjf3G2haIyW1x4V06HCN0az5uBrROFYCYyIfuTnu tumble@latias"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA+ufH9Z7saRZ3UJgKF2XgXX0SRvRPb62II6co3gcjS7 u0_a399@localhost"
      ];
    };
    fisheater = {
      isNormalUser = true;
      description = "Fisheater";
      #openssh.authorizedKeys.keys = [];
    };
    pankit = {
      isNormalUser = true;
      description = "Pankit";
      #openssh.authorizedKeys.keys = [];
    };
  };

  # DON'T TOUCH
  system.stateVersion = "23.05";
}
