{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.official.systemBase;
in {
  options.official.systemBase = {
    owner = mkOption {
      type = types.str;
      example = "SArpnt";
      description = lib.mdDoc "Name of person who owns the system.";
    };
    sshPorts = mkOption {
      type = types.listOf types.port;
      default = [22];
      description = lib.mdDoc "list of TCP ports to use for ssh";
    };
    moshPorts = mkOption {
      type = types.attrsOf types.port;
      default = {
        from = 60000;
        to = 61000;
      };
      description = lib.mdDoc "UDP port range for mosh, start inclusive";
    };
  };
  config = {
    services.logind.lidSwitch = "ignore"; # don't suspend on lid close

    nix.gc.automatic = true; # clean up garbage files

    # NOTE: doas doesnt work yet, https://github.com/NixOS/nixpkgs/issues/251254
    #security.sudo.enable = false;
    #security.doas.enable = true;
    #security.doas.extraRules = [
    #	{ groups = ["wheel"]; persist = true; }
    #];

    #security.rtkit.enable = true; # TODO would this be useful?

    programs.fish.enable = true;
    programs.fish.promptInit = ''
      ${pkgs.any-nix-shell}/bin/any-nix-shell fish --info-right | source
    '';
    users.defaultUserShell = pkgs.fish;

    users.motd = ''
      ${config.networking.hostName}, owned by ${cfg.owner} in ${config.time.timeZone}, running NixOS

      search packages:
      	`nix search (package)` TODO doesn't work
      system configuration:
      	/etc/nixos/configuration.nix
      current system configuration:
      	/run/current-system/configuration.nix
      upgrading:
      	`sudo nix-channel --add https://channels.nixos.org/nixos-(LATEST STABLE)-small nixos`
      	`sudo nixos-rebuild switch --upgrade`
      delete old version backups:
      	``
      deduplicate nix store (resource intensive!):
      	`sudo nix store optimise` TODO doesn't work
    '';

    #users.ldap.enable = true; # TODO would this be useful?

    programs.git.enable = true;

    # TODO rsync?

    environment.systemPackages = with pkgs; [
      #pkgs.symlinkJoin { name = "doas" "exec sudo" # TODO symlink sudo/doas
      (pkgs.runCommand "mosh" {
          buildInputs = [pkgs.makeWrapper];
        } ''
          mkdir $out
          ln -s ${pkgs.mosh}/* $out
          rm $out/bin
          mkdir $out/bin
          ln -s ${pkgs.mosh}/bin/* $out/bin
          rm $out/bin/mosh-server
          makeWrapper ${pkgs.mosh}/bin/mosh-server $out/bin/mosh-server \
          	--append-flags "-p ${toString cfg.moshPorts.from}:${toString cfg.moshPorts.to}"
        '')
      #(pkgs.runCommand "motd" {} ''
      #  echo ${users.motd}
      #'')
      neofetch
      tealdeer
      nano
      forkstat
      ripgrep
      psmisc
    ];

    # Some programs need SUID wrappers, can be configured further or are
    # started in user sessions.
    # programs.mtr.enable = true;
    #programs.gnupg.agent = {
    #	enable = true;
    #	enableSSHSupport = true;
    #};

    # mosh
    networking.firewall.allowedUDPPortRanges = [cfg.moshPorts];
    security.wrappers.utempter = {
      source = "${pkgs.libutempter}/lib/utempter/utempter";
      owner = "root";
      group = "utmp";
      setuid = false;
      setgid = true;
    };

    services.openssh = {
      enable = true;
      openssh.ports = cfg.sshPorts;
      openssh.settings = {
        AllowGroups = ["users"];
        PermitRootLogin = "no";
        PasswordAuthentication = false;
        KbdInteractiveAuthentication = false;
      };
    };

    # Copy the NixOS configuration file and link it from the resulting system
    # (/run/current-system/configuration.nix). This is useful in case you
    # accidentally delete configuration.nix.
    # system.copySystemConfiguration = true;
  };
}
