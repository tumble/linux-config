{disks ? ["/dev/sda" "/dev/sdb"], ...}:
with builtins; let
  dev-os = elemAt disks 0;
  dev-tmbsrv-cache = elemAt disks 1;
  dev-tmbsrv-1 = elemAt disks 2;
  dev-tmbsrv-2 = elemAt disks 3;
  dev-tmbsrv-3 = elemAt disks 4;
  dev-tmbsrv-4 = elemAt disks 5;
  dev-tmbsrv-5 = elemAt disks 6;
in {
  disko.devices = {
    nodev = {
      "/" = {
        fsType = "tmpfs";
        mountOptions = ["mode=755"];
        #mountOptions = [ "size=1024M" ];
      };
      "/tmp" = {
        fsType = "tmpfs";
      };
    }; # </nodev>

    disk = {
      tmbsrv_os = {
        device = dev-os;
        content = {
          type = "gpt";
          partitions = {
            nix = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = ["-f"];
                subvolumes = {
                  "/root" = {
                    mountpoint = "/persist";
                    mountOptions = ["subvol=root" "compress=zstd" "noatime"];
                  };
                  "/nix" = {
                    mountpoint = "/nix";
                    mountOptions = ["subvol=nix" "compress=zstd" "noatime"];
                  };
                };
              };
            };

            swap = {
              start = "-100G";
              content = {
                type = "swap";
                randomEncryption = true;
                #resumeDevice = true;
              };
            }; # </swap>

            boot = {
              start = "-500M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = ["umask=027" "gid=1"]; # readable by wheel group
              };
            }; # </boot>
          }; # </partitions>
        };
      }; # </main>

      tmbsrv-cache = {
        type = "disk";
        device = dev-tmbsrv-cache;
        content = {
          type = "gpt";
          partitions = {
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "skydrive";
              };
            };
          };
        };
      };

      tmbsrv-1 = {
        type = "disk";
        device = dev-tmbsrv-1;
        content = {
          type = "gpt";
          partitions = {
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "skydrive";
              };
            };
          };
        };
      };

      tmbsrv-2 = {
        type = "disk";
        device = dev-tmbsrv-2;
        content = {
          type = "gpt";
          partitions = {
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "skydrive";
              };
            };
          };
        };
      };

      tmbsrv-3 = {
        type = "disk";
        device = dev-tmbsrv-3;
        content = {
          type = "gpt";
          partitions = {
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "skydrive";
              };
            };
          };
        };
      };

      tmbsrv-4 = {
        type = "disk";
        device = dev-tmbsrv-4;
        content = {
          type = "gpt";
          partitions = {
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "skydrive";
              };
            };
          };
        };
      };

      tmbsrv-5 = {
        type = "disk";
        device = dev-tmbsrv-5;
        content = {
          type = "gpt";
          partitions = {
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "skydrive";
              };
            };
          };
        };
      };
    };
    zpool.skydrive = {
      type = "zpool";
      rootFsOptions = {
        canmount = "off";
        compression = "zstd";
      };
      mode.topology = {
        type = "topology";
        cache = ["tmbsrv-cache"];
        vdev = [
          {
            mode = "raidz1";
            members = ["tmbsrv-1" "tmbsrv-2" "tmbsrv-3" "tmbsrv-4" "tmbsrv-5"];
          }
        ];
      };
      datasets = {
        "home" = {
          type = "zfs_fs";
          mountpoint = "/home";
        };
        "archive" = {
          type = "zfs_fs";
          mountpoint = "/archive";
        };
        "var" = {
          type = "zfs_fs";
          mountpoint = "/var";
        };
      };
    };
  };
}
