{
  self,
  inputs,
  config,
  lib,
  pkgs,
  ...
}: {
  # VERSION AT INSTALL
  system.stateVersion = "23.11";

  # TEMP until hardwareconfig is made
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";

  services.caddy.enable = true;

  imports = [
    inputs.disko.nixosModules.default
    inputs.ficial-config.outPath
    (import ./disko.nix {
      devices = [
        "/dev/disk/by-id/wwn-0x500a0751e5f7b990"
        "/dev/disk/by-id/wwn-0x57c35481d8e4f34e"
        "/dev/disk/by-id/wwn-0x5000039fdef5d29d"
        "/dev/disk/by-id/wwn-0x5000039fdef6db49"
        "/dev/disk/by-id/wwn-0x5000039fdef6db49"
        "/dev/disk/by-id/wwn-0x50014ee26872854e"
        "/dev/disk/by-id/wwn-0x50014ee00472ffa3"
      ];
    })
  ];

  tumble-config = {
    updates.enable = true;
    home-lan.enable = true;
    sops.enable = true;
    interface = "enp5s0";
    headless = true;
    impermanence = {
      enable = true;
      path = "/persist";
      persistDefault = false;
      home.enable = false;
      files = [
        #"/etc/passwd"
        #"/etc/shadow"
        #"/etc/group"
      ];
      directories = [
        #"/etc/ssh"
        #"/etc/samba"
        "/etc"
      ];
    };
    servers = {
      ssh.enable = true;
      jellyfin.enable = true;
      samba.enable = true;
      radicale.enable = true;
      ente.enable = true;
      mediawiki.enable = false;
      syncthing.enable = true;
      game-with-chat.enable = true;
      pihole.enable = false;
      peertube.enable = true;
      home-assistant.enable = true;
    };
  };

  official.systemBase = {
    enable = true;
    owner = "Tumble";
    # sshPorts = lib.mkForce [3023]
    #moshPorts = {
    #	from = 60500;
    #	to = 60600;
    #};
  };

  # make sure that we can use zfs
  #boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages; # deprecated
  services.zfs.autoScrub.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.groups.archive.members = ["tumble" "sarpnt" "dadlish"];
  users.users = {
    tumble = {
      isNormalUser = true;
      extraGroups = ["wheel" "archive"];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJWyAQVqlP4UGZiq8CjrDCxxmd/fn1PG5BB097qwmCL0 tumble@tumble-pc"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJtO/JHNi7Jj8LiOqkGIViC3S8kqirEDLAJi3GRepiGt tumble@TUMBLE-WIN"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEKLQdjayaFx7gvYmJWs2pXId+oh08m/SVyqZxKQNlCo tumble@latias"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA+ufH9Z7saRZ3UJgKF2XgXX0SRvRPb62II6co3gcjS7 u0_a399@localhost"
      ];
      hashedPassword = "$y$j9T$wrqA.yNCCkrlVCBFioz/l.$bAghMraXXKDoe4RAcmp.aJSEGDptyr0YbTCzvNHtzA4";
    };
    sarpnt = {
      isNormalUser = true;
      extraGroups = ["wheel" "archive"];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA/trguK8JO48Bxv8YCiPW+IqRSa50DqjnsklV3C2foI sarpnt@ace"
      ];
    };
    dadlish = {
      isNormalUser = true;
      extraGroups = ["wheel" "archive"];
      #openssh.authorizedKeys.keys = [];
    };
    jellyfin = {
      extraGroups = ["archive"];
    };
    edcdecl = {
      isNormalUser = true;
      extraGroups = ["archive"];
    };
  };

  networking = {
    hostName = "onyx";
    # TODO: run `head -c 8 /etc/machine-id` on the system
    hostId = "7e9b2530";
    nameservers = ["9.9.9.9"];
    #networkmanager.enable = true;
    defaultGateway = {
      address = "192.168.0.1";
      interface = "enp5s0";
    };

    # interfaces.enp5s0 = {
    # 	ipv4.addresses = [
    # 		{
    # 			address = "192.168.0.70";
    # 			prefixLength = 24;
    # 		}
    # 	];
    # };
  };
}
