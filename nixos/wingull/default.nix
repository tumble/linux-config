# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual ().
{
  config,
  lib,
  pkgs,
  self,
  modulesPath,
  ...
}: {
  imports = [
    (modulesPath + "/installer/cd-dvd/installation-cd-minimal.nix")
  ];
  system.stateVersion = "25.05";

  environment.systemPackages = with pkgs; [
    # self.packages.${pkgs.system}.tumble-install
    (writeScriptBin "tumble-install" ''
      nix --extra-experimental-features "nix-command flakes" run git+https://git.disroot.org/tumble/linux-config?ref=nixos#tumble-install
    '')
  ];
  isoImage.squashfsCompression = "gzip -Xcompression-level 1";

  tumble-config = {
    updates.enable = false;
    # desktop = "plasma";
    nm = true;
  };
  networking.hostName = "wingull";
  networking.wireless.enable = false;
}
