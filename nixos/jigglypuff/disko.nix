{disks ? ["/dev/sda" "/dev/sdb"], ...}:
with builtins; let
  device0 = elemAt disks 0;
in {
  disko.devices = {
    nodev = {
      "/tmp" = {
        fsType = "tmpfs";
      };
    }; # </nodev>
    disk = {
      main = {
        device = device0;
        content = {
          type = "gpt";
          partitions = {
            nix = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = ["-f"];
                subvolumes = {
                  "/nix" = {
                    mountpoint = "/nix";
                    mountOptions = ["subvol=nix" "compress=zstd" "noatime"];
                  };

                  "/root" = {
                    mountpoint = "/";
                    mountOptions = ["subvol=root" "compress=zstd" "noatime"];
                  };
                  "/home" = {
                    mountpoint = "/home";
                    mountOptions = ["subvol=home" "compress=zstd" "noatime"];
                  };
                };
              };
            }; # </nix>

            swap = {
              start = "-16G";
              content = {
                type = "swap";
                #randomEncryption = true;
                resumeDevice = true;
              };
            }; # </swap>

            boot = {
              start = "-500M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = ["umask=027" "gid=1"];
              };
            }; # </boot>
          }; # </partitions>
        };
      }; # </main>
    };
  };
}
