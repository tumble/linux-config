{inputs, ...}: {
  imports = [
    inputs.disko.nixosModules.default
    (import ./disko.nix {
      devices = [
        "/dev/disk/by-id/wwn-0x500a0751e6ab378d"
      ];
    })
    ./hardware-configuration.nix
  ];
  system.stateVersion = "25.05";

  tumble-config = {
    interface = "enp3s0";
    desktop = "plasma";
    nm = true;
    projects.enable = true;
    updates.enable = true;
  };

  users.users.tumble = {
    isNormalUser = true;
    extraGroups = ["wheel" "input" "kvm" "disk" "libvirtd" "audio" "video" "networkmanager"];
    hashedPassword = "$y$j9T$wrqA.yNCCkrlVCBFioz/l.$bAghMraXXKDoe4RAcmp.aJSEGDptyr0YbTCzvNHtzA4";
  };

  networking.hostName = "jigglypuff";

  services.printing.enable = true;
}
