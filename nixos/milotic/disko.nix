{disks ? ["/dev/sda" "/dev/sdb"], ...}:
with builtins; let
  device0 = elemAt disks 0;
  device1 = elemAt disks 1;
in {
  disko.devices = {
    nodev = {
      "/tmp" = {
        fsType = "tmpfs";
      };
    }; # </nodev>
    disk = {
      main = {
        device = device0;
        content = {
          type = "gpt";
          partitions = {
            boot = {
              size = "500M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = ["umask=027" "gid=1"];
              };
            }; # </boot>
            nix = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = ["-f"];
                subvolumes = {
                  "/nix" = {
                    mountpoint = "/nix";
                    mountOptions = ["subvol=nix" "compress=zstd" "noatime"];
                  };

                  "/root" = {
                    mountpoint = "/";
                    mountOptions = ["subvol=root" "compress=zstd" "noatime"];
                  };
                };
              };
            }; # </nix>
          }; # </partitions>
        };
      }; # </main>
      home = {
        device = device1;
        content = {
          type = "gpt";
          partitions = {
            swap = {
              size = "38G";
              content = {
                type = "swap";
                #randomEncryption = true;
                resumeDevice = true;
              };
            }; # </swap>
            home = {
              size = "100%";
              content = {
                type = "btrfs";
                subvolumes."/home" = {
                  mountpoint = "/home";
                  mountOptions = ["compress=zstd"];
                };
              };
            }; # </home>
          };
        };
      }; # </home>
    };
  };
}
