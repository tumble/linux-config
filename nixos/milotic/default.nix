{
  self,
  inputs,
  config,
  lib,
  pkgs,
  ...
}: {
  # VERSION AT INSTALL
  system.stateVersion = "23.11";

  imports = [
    inputs.disko.nixosModules.default
    (import ./disko.nix {
      devices = [
        "/dev/disk/by-id/nvme-Patriot_M.2_P300_128GB_P300NDBB24110702356"
        "/dev/disk/by-id/nvme-KINGSTON_SNV3S1000G_5002687383829E5D"
      ];
    })
    ./hardware-configuration.nix
  ];

  tumble-config = {
    desktop = "plasma";
    home-lan.enable = true;
    gaming.enable = true;
    servers.ssh.enable = true;
    htpc = {
      enable = true;
      dvd-drive = true;
      amd-gpu = true;
      android = true;
    };
    updates.enable = true;
  };

  networking.hostName = "milotic";
  boot.kernelParams = ["usbcore.autosuspend=-1"];
}
