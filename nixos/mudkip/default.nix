{
  self,
  inputs,
  config,
  lib,
  pkgs,
  ...
}: {
  # VERSION AT INSTALL
  system.stateVersion = "23.11";

  imports = [
    inputs.disko.nixosModules.default
    (import ./disko.nix {devices = ["/dev/disk/by-id/wwn-0x5000cca756ca3ffc"];})
    ./hardware-configuration.nix
    inputs.jovian.nixosModules.jovian
  ];

  tumble-config = {
    desktop = "kodi";
    updates.enable = true;
    impermanence.enable = true;
    htpc.enable = true;
  };

  services.logind.powerKey = "suspend";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.mutableUsers = false;
  services.logind.lidSwitch = "ignore";

  services.getty.autologinUser = "tv";

  networking = {
    hostName = "mudkip";
    nameservers = ["9.9.9.9"];
    networkmanager.enable = true;
  };

  environment.persistence."/storage/root".directories = lib.mkIf config.tumble-config.impermanence.enable [
    "/etc/NetworkManager/system-connections"
  ];

  networking.firewall = {
    enable = lib.mkDefault true;
    allowedTCPPorts = [8080 137 138];
    allowedUDPPorts = [8080 137 138];
  };
  boot.kernelParams = ["usbcore.autosuspend=-1"];
}
