{disks ? ["/dev/sda" "/dev/sdb"], ...}:
with builtins; let
  device0 = elemAt disks 0;
  device1 = elemAt disks 1;
in {
  disko.devices = {
    nodev = {
      "/" = {
        fsType = "tmpfs";
        mountOptions = ["mode=755"];
        #mountOptions = [ "size=1024M" ];
      };
      "/tmp" = {
        fsType = "tmpfs";
      };
    }; # </nodev>
    disk = {
      main = {
        device = device0;
        content = {
          type = "gpt";
          partitions = {
            nix = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = ["-f"];
                subvolumes = {
                  "/nix" = {
                    mountpoint = "/nix";
                    mountOptions = ["subvol=nix" "compress=zstd" "noatime"];
                  };

                  "/root" = {
                    mountpoint = "/storage/root";
                    mountOptions = ["subvol=root" "compress=zstd" "noatime"];
                  };
                  "/home" = {
                    mountpoint = "/storage/home";
                    mountOptions = ["compress=zstd"];
                  };
                };
              };
            }; # </nix>

            swap = {
              start = "-100G";
              content = {
                type = "swap";
                #randomEncryption = true;
                resumeDevice = true;
              };
            }; # </swap>

            boot = {
              start = "-500M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = ["umask=027" "gid=1"];
              };
            }; # </boot>
          }; # </partitions>
        };
      }; # </main>
    };
  };
}
