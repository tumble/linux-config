{
  description = "Nixos config flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
    unity.url = "github:nixos/nixpkgs/6adf48f53d819a7b6e15672817fa1e78e5f4e84f";

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    impermanence.url = "github:nix-community/impermanence";
    preservation.url = "github:nix-community/preservation";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    jovian = {
      url = "github:/Jovian-Experiments/Jovian-NixOS";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    #firefox-addons = {
    #	url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
    #	inputs.nixpkgs.follows = "nixpkgs";
    #};
    hyprland.url = "github:hyprwm/Hyprland";
    hyprland-plugins = {
      url = "github:hyprwm/hyprland-plugins";
      inputs.hyprland.follows = "hyprland"; # Prevents version mismatch.
    };

    nixos-cosmic = {
      url = "github:lilyinstarlight/nixos-cosmic";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    tumblepkgs = {
      url = "git+https://git.disroot.org/tumble/tumblepkgs";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ficial-config = {
      url = "git+https://git.disroot.org/ficial/nix-config";
      flake = false;
    };

    gwc = {
      url = "github:game-with-chat/proto/develop";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    home-manager,
    nixpkgs,
    tumblepkgs,
    gwc,
    flake-utils,
    ...
  } @ inputs: let
    lib = nixpkgs.lib // home-manager.lib;

    moduleArgs = {inherit self inputs;};

    nixos = with builtins;
      mapAttrs
      (name: type: ./nixos + "/${name}")
      (readDir ./nixos);

    homes = with builtins;
      listToAttrs (map
        (name: {
          name = lib.removeSuffix ".nix" name;
          value = ./home + "/${name}";
        }) (attrNames (readDir ./home)));

    packages = with builtins;
      listToAttrs (map
        (name: {
          name = lib.removeSuffix ".nix" name;
          value = ./pkgs + "/${name}";
        }) (attrNames (readDir ./pkgs)));

    mkPkgs = system:
      import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
  in
    inputs.flake-utils.lib.eachDefaultSystem (system: let
      pkgs = mkPkgs system;
    in {
      packages =
        builtins.mapAttrs (
          name: config: pkgs.callPackage config moduleArgs
        )
        packages;
      formatter = pkgs.alejandra;
    })
    // inputs.flake-utils.lib.eachDefaultSystemPassThrough (system: {
      overlays.default = final: prev: {
        gwc = gwc.defaultPackage.${prev.system};
        #   flatland-xr = stardust-flatland.packages.${system}.default;
        yargNightly = prev.yarg.overrideAttrs (oldAttrs: rec {
          version = "b2876";

          src = prev.fetchzip {
            url = "https://github.com/YARC-Official/YARG-BleedingEdge/releases/download/${version}/YARG_${version}-Linux-x64.zip";
            stripRoot = false;
            hash = "sha256-WMAm/S/8RBRYyd4C+hkaZsKbcypZYNSLcBWmyLer2Pk=";
          };

          meta =
            oldAttrs.meta
            // {
              homepage = "https://github.com/YARC-Official/YARG-BleedingEdge";
              description = "Free, open-source, plastic guitar game (bleeding-edge, version ${version})";
            };
        });
      };

      nixosConfigurations = builtins.mapAttrs (hostname: config:
        lib.nixosSystem {
          inherit system;
          specialArgs = moduleArgs;
          modules = [
            self.nixosModule
            config
          ];
        })
      nixos;

      homeConfigurations = builtins.mapAttrs (user: config:
        lib.homeManagerConfiguration {
          extraSpecialArgs = moduleArgs;
          modules = [
            self.homeManagerModule
            config
          ];
          pkgs = mkPkgs system;
        })
      homes;
    })
    // (let
      sshBase = 22000;
      systemIps = {
        homestead = 1;
        latias = 8;
        milotic = 30;
        jirachi = 33;
        porygon = 44;
        suicune = 46;
        alakazam = 67;
        onyx = 70;
      };
      mapHosts = ipMap:
        builtins.mapAttrs (name: ipSuffix: rec {
          inherit name;
          ip = "192.168.0.${toString ipSuffix}";
          fqdn = "${name}.local";
          ssh = sshBase + ipSuffix * 10;
          moshStart = ssh + 1;
          moshEnd = ssh + 9;
        })
        ipMap;
      hostsSet = mapHosts systemIps;
    in {
      inherit hostsSet;
      hostsList = builtins.attrValues hostsSet;
      nixosModule = import ./modules/nixos;
      homeManagerModule = import ./modules/home;
      templates = import ./templates lib;
    });
}
