#! /usr/bin/env nix-shell 
#! nix-shell -i bash -p util-linux udisks gawk

WINE=/run/current-system/sw/bin/wine
KODI=/run/current-system/sw/bin/kodi

# Detect the DVD drive
DEVICE=$(lsblk -o NAME,TYPE | grep rom | awk '{print "/dev/"$1}')

if [ -z "$DEVICE" ]; then
    echo "No DVD drive detected."
    exit 1
fi

MOUNT_POINT=$(lsblk -n -o MOUNTPOINT $DEVICE)

if ! mountpoint -q "$MOUNT_POINT"; then
    echo "Mounting $DEVICE..."
    udisksctl mount -b "$DEVICE" || { echo "Failed to mount DVD."; exit 1; }
	MOUNT_POINT=$(lsblk -n -o MOUNTPOINT $DEVICE)
fi

if [ -d "$MOUNT_POINT/VIDEO_TS" ]; then
    echo "Media DVD detected. Launching Kodi..."
    $KODI dvd://"$DEVICE"
    udisksctl unmount -b "$DEVICE"
    exit 0
fi

if [ -f "$MOUNT_POINT/autorun.inf" ]; then
    echo "autorun.inf found. Reading..."
    
    OPEN_CMD=$(grep -i '^open=' "$MOUNT_POINT/autorun.inf" | cut -d '=' -f2 | tr -d '\r')

    if [ -n "$OPEN_CMD" ]; then
        GAME_EXE="$MOUNT_POINT/$OPEN_CMD"
        echo "Autorun specifies: $GAME_EXE"

        # Check if the executable exists
        if [ -f "$GAME_EXE" ]; then
            echo "Running game with Wine..."
            $WINE "$GAME_EXE"
            udisksctl unmount -b "$DEVICE"
            exit 0
        else
            echo "Executable in autorun.inf not found!"
        fi
    fi
fi

# If nothing matched
echo "Unknown DVD type."
exit 1
