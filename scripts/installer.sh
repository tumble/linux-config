#! /usr/bin/env nix-shell 
#! nix-shell -i bash -p fzf newt dialog

SETUP_HOSTNAME=$HOSTNAME
SETUP_OSDRIVE=""
SETUP_HOMEDRIVE=""
SETUP_RAIDDRIVES=()
SETUP_RAIDCACHE=""
SETUP_RAIDMODE=""
SETUP_RAIDNAME=""
SETUP_DESKTOP=""
SETUP_NETWORK=""
SETUP_HOME=1
SETUP_FEATURES=()
SETUP_SERVERS=()
SETUP_SWAP=$(lsmem | awk '/Total online memory:/ {print $4*2}'  )
SETUP_ERROR="Welcome, please setup the things that you want to setup"

if [ -f tumble-setup.env ]; then
    source tumble-setup.env
fi



while true; do
    CHOICE=$(cat << EOF | fzf --layout=reverse --header="$SETUP_ERROR" | awk -F' - ' '{print $1}'
Hostname - $SETUP_HOSTNAME
Swap - $(echo $SETUP_SWAP)
OS Drive - $SETUP_OSDRIVE
Home Drive - $SETUP_HOMEDRIVE
Raid Drives - $(echo ${SETUP_RAIDDRIVES[@]})
Raid Cache - $(echo $SETUP_RAIDCACHE)
Raid Mode - $(echo $SETUP_RAIDMODE)
Raid Name - $(echo $SETUP_RAIDNAME)
Desktop - $SETUP_DESKTOP
Network Interface - $SETUP_NETWORK
At Home? - $SETUP_HOME
Features - $(echo ${SETUP_FEATURES[@]})
Servers - $(echo ${SETUP_SERVERS[@]})
[Install]
EOF
)

SETUP_ERROR=""

    case "$CHOICE" in
        "Hostname")
			SETUP_HOSTNAME=$(whiptail --inputbox "Hostname" 8 40 3>&1 1>&2 2>&3)
            ;;
        "Swap")
			SETUP_SWAP=$(whiptail --inputbox "Swap partition size" 8 40 3>&1 1>&2 2>&3)
            ;;
        "OS Drive")
			SETUP_OSDRIVE=$(lsblk -Sno ID-LINK,NAME,MODEL,TRAN,SIZE|fzf --header="Select a device" --layout=reverse --ansi | awk '{print "/dev/disk/by-id/" $1}' )
            ;;
        "Home Drive")
			SETUP_HOMEDRIVE=$(lsblk -Sno ID-LINK,NAME,MODEL,TRAN,SIZE|fzf --header="Select a device" --layout=reverse --ansi | awk '{print "/dev/disk/by-id/" $1}' )
            ;;
        "Raid Drives")
			SETUP_RAIDDRIVES=($(lsblk -Sno ID-LINK,NAME,MODEL,TRAN,SIZE|fzf --header="Select a device (press TAB to highlight)" --layout=reverse --ansi --multi | awk '{print "/dev/disk/by-id/" $1}' ))
            ;;
        "Raid Cache")
			SETUP_RAIDCACHE=$(lsblk -Sno ID-LINK,NAME,MODEL,TRAN,SIZE|fzf --header="Select a device" --layout=reverse --ansi | awk '{print "/dev/disk/by-id/" $1}' )
            ;;
        "Raid Name")
			SETUP_RAIDNAME=$(whiptail --inputbox "Raid Name" 8 40 3>&1 1>&2 2>&3)
            ;;
		"Raid Mode")
			    SETUP_RAIDMODE=$(cat << EOF | fzf --layout=reverse
mirror
raidz1
raidz2
raidz3
EOF
				)
            ;;
        "Desktop")
			SETUP_DESKTOP=$(ls $(dirname $0)/../modules/nixos/desktops/ | grep -v "default.nix"| awk -F '.nix' '{print $1}' | fzf --header="Select a Desktop" --layout=reverse) 
            ;;
        "Network Interface")
			SETUP_NETWORK=$(ls /sys/class/net/ | fzf --header="Select network adapter" --layout=reverse)
            ;;
        "At Home?")
			if [ "$SETUP_HOME" -EQ 0 ]; then
				SETUP_HOME=1
			else
				SETUP_HOME=0
				fi
            ;;
        "Features")
			    SETUP_FEATURES=($(cat << EOF | fzf --layout=reverse --multi
gaming
emulation
streaming
vr
capture-card
steam-presense
wifi
wifi-alt
projects
obs
unity
EOF
				))
            ;;
        "Servers")
			SETUP_SERVERS=($(ls $(dirname $0)/../modules/nixos/servers/ | grep -v "default.nix"| awk -F '.nix' '{print $1}' | fzf --multi --layout=reverse))
            ;;
		"[Install]")
				if [ -z "$SETUP_HOSTNAME" ]; then
					SETUP_ERROR="Please set a hostname"
				fi

				if [ -z "$SETUP_OSDRIVE" ]; then
					SETUP_ERROR="Please set an os drive"
				fi

				if [ -z "$SETUP_NETWORK" ]; then
					SETUP_ERROR="Please set an network interface"
				fi
				if [ -z "$SETUP_RAIDDRIVES" ] && [ -n "$SETUP_RAIDCACHE" ]; then
					SETUP_ERROR="Please set raid drives"
				fi

				if [ -z "$SETUP_ERROR" ]; then
					break
				fi
			;;
        *)
           echo "Aborting..."
			exit
            ;;
    esac
done




set | grep SETUP_ |grep -vE '=$'> tumble-setup.env
# done



# display partition layout based on selection
cat <<EOF
PARTITIONS

Temp FS:
nodev	tmpfs
			/
			/tmp

OS Drive:
$(lsblk -Sno TRAN,MODEL,SIZE "$SETUP_OSDRIVE")	
	fat32	/boot		500 MB
	swap	swap		${SETUP_SWAP}G
	btfs
		/persist
		/nix
EOF

# If no home or raid drive is defined, put /home under OS DRIVE
if [[ -z "$SETUP_HOMEDRIVE" && -z "$SETUP_RAIDDRIVES" ]]; then
echo "		/home"
fi

# Show home drive if defined
if [[ -n "$SETUP_HOMEDRIVE" ]]; then
    cat <<EOF

Home Drive:
$(lsblk -Sno TRAN,MODEL,SIZE "$SETUP_HOMEDRIVE")	
	btfs
			/home
EOF
fi

# Show RAID drive if defined
if [[ -n "$SETUP_RAIDDRIVES" ]]; then
    cat <<EOF

Raid Drives:
$(lsblk -Sno TRAN,MODEL,SIZE "$SETUP_RAIDDRIVES")
$SETUP_RAIDNAME	zfs	$SETUP_RAIDMODE
		/archive
		/var
EOF

    # If no home drive is defined, put /home under RAID DRIVES
    if [[ -z "$SETUP_HOMEDRIVE" ]]; then
        echo "		/home"
    fi
fi

# ask the user if they are sure

# tell the user that this will wipe everything


DISKO_DRIVE_RAIDCACHE_NUM=1
DISKO_DRIVE_RAID_NUM=1
NIXCONF_DISKO_LIST="\"$SETUP_OSDRIVE\""


# write disko script

if [ -z $SETUP_HOMEDRIVE ]; then
NIXCONF_DISKO_LIST+=" \"$SETUP_HOMEDRIVE\""
DISKO_OSHOME=('
                  "/home" = {
				  #TODO: do storage/home if impermenance
                    mountpoint = "/home";
                    mountOptions = ["compress=zstd"];
                  };')
else
DISKO_DRIVE_RAIDCACHE_NUM=2
DISKO_DRIVE_RAID_NUM=2
DISKO_DRIVE_HOME='device_home = elemAt disks 1;'
	DISKO_HOME=('home = {
        device = device_home;
        content = {
          type = "gpt";
          partitions.home.content = {
            type = "btrfs";
            subvolumes."/home" = {
              mountpoint = "/home";
              mountOptions = ["compress=zstd"];
            };
          }; # </home>
        };')
fi

if [ -n "$SETUP_RAIDCACHE" ]; then
NIXCONF_DISKO_LIST+=" \"$SETUP_RAIDCACHE\""
((DISKO_DRIVE_RAID_NUM++))
DISKO_DRIVE_RAIDCACHE="dev-tmbraid-cache = elemAt disks ${DISKO_DRIVE_RAIDCACHE_NUM};"
DISKO_RAIDCACHE=('tumbleraid-cache = {
        type = "disk";
        device = dev-tmbraid-cache;
        content = {
          type = "gpt";
          partitions = {
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "${SETUP_RAIDNAME}";
              };
            };
          };
        };
      };')
fi

if [ -n "$SETUP_RAIDDRIVES" ]; then
	DISKO_RAIDDISKS=""
	DISKO_DRIVE_RAID=""
	DISKO_RAID_MEMBERS=""
	for n in "${!SETUP_RAIDDRIVES[@]}"; do
		NIXCONF_DISKO_LIST+=" \"${SETUP_RAIDDRIVES[$n]}\""
		DISKO_DRIVE_RAID="$DISKO_DRIVE_RAID
		dev-tmbraid-${n} = elemAt disks $(((DISKO_DRIVE_RAID_NUM + n)));"
		DISKO_RAID_MEMBERS="$DISKO_RAID_MEMBERS} \"tumbleraid-${n}\""
		DISKO_RAIDDISKS="$DISKO_RAIDDISKS
		tumbleraid-${n} = {
        type = \"disk\";
			device = dev-tmbraid-${n};
			content = {
			type = \"gpt\";
			partitions = {
				zfs = {
				size = \"100%\";
				content = {
					type = \"zfs\";
					pool =\"${SETUP_RAIDNAME}\";
				};
				};
			};
        };
      };"
	done

	#cache = ["tmbsrv-cache"];


        # "home" = {
        #   type = "zfs_fs";
        #   mountpoint = "/home";
        # };

	DISKO_RAID='zpool.${SETUP_RAIDNAME} = {
      type = "zpool";
      rootFsOptions = {
        canmount = "off";
        compression = "zstd";
      };
      mode.topology = {
        type = "topology";
		$DISKO_RAIDTOP_CACHE
        vdev = [
          {
            mode = "raidz1";
            members = [${DISKO_RAID_MEMBERS} ];
          }
        ];
      };
      datasets = {
	  	$DISKO_RAIDDS_HOME
        "archive" = {
          type = "zfs_fs";
          mountpoint = "/archive";
        };
        "var" = {
          type = "zfs_fs";
          mountpoint = "/var";
        };
      };
    };'
fi


cat << EOF > disko.nix
{disks ? ["/dev/sda" "/dev/sdb"], ...}:
with builtins; let
  device_os = elemAt disks 0;
  $DISKO_DRIVE_HOME
  $DISKO_DRIVE_RAIDCACHE
  $DISKO_DRIVE_RAID
in {
  disko.devices = {
    nodev = {
      "/" = {
        fsType = "tmpfs";
        mountOptions = ["mode=755"];
        #mountOptions = [ "size=1024M" ];
      };
      "/tmp" = {
        fsType = "tmpfs";
      };
    }; # </nodev>

	disk = {
      main = {
        device = device_os;
        content = {
          type = "gpt";
          partitions = {
            nix = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = ["-f"];
                subvolumes = {
                  "/nix" = {
                    mountpoint = "/nix";
                    mountOptions = ["subvol=nix" "compress=zstd" "noatime"];
                  };
                  "/root" = {
                    mountpoint = "/persist";
                    mountOptions = ["subvol=root" "compress=zstd" "noatime"];
                  };$DISKO_OSHOME
                };
              };
            }; # </nix>

            swap = {
              start = "-${SETUP_SWAP}G";
              content = {
                type = "swap";
                #randomEncryption = true;
                resumeDevice = true;
              };
            }; # </swap>

            boot = {
              start = "-500M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = ["umask=027" "gid=1"];
              };
            }; # </boot>
          }; # </partitions>
        };
      }; # </main>
	  $DISKO_HOME
	  $DISKO_RAIDCACHE
	  $DISKO_RAIDDISKS
    };
  };
  $DISKO_RAID
}
EOF


nixos-generate-config --no-filesystems --show-hardware-config > hardware-configuration.nix


cat << EOF > default.nix
# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (\`nixos-help\`).

{ config, inputs, lib, pkgs, ... }:

{
  imports =
    [
        inputs.disko.nixosModules.default
    (import ./disko.nix {devices = [$NIXCONF_DISKO_LIST];})
    ./hardware-configuration.nix
    ];
  system.stateVersion = "$(nixos-version | awk -F'.' '{print $1 "." $2}')";

  tumble-config = {
  
  };
}
EOF

echo ""
read -rp "Is this correct (Y/N)?" response

if [[ "${response,,}" == "yes" || "${response,,}" == "y" || "${response,,}" == "1" ]]; then
echo ""
echo ""
		echo "WARNING: ALL DATA ON THE FOLLING DISKS WILL BE LOST"
		lsblk -Sno ID-LINK,NAME,MODEL,TRAN,SIZE $SETUP_OSDRIVE $SETUP_HOMEDRIVE $SETUP_RAIDCACHE $SETUP_RAIDDRIVES
		read -rp "Proceed with format and nixos-install (Y/N)?" response

		if [[ "${response,,}" == "yes" || "${response,,}" == "y" || "${response,,}" == "1"  ]]; then
			echo "Continuing..."
		else
			echo "Aborting script."
			exit 0
		fi
else
    echo "Aborting script."
    exit 0
fi

echo "====> Formatting and partitioning disks using Disko..."
disks="["
disks+="\"$SETUP_OSDRIVE\""
if [[ -n "$SETUP_HOMEDRIVE" ]]; then
    disks+=", \"$SETUP_HOMEDRIVE\""
fi
if [[ -n "$SETUP_RAIDCACHE" ]]; then
    disks+=", \"$SETUP_RAIDCACHE\""
fi
for drive in "${SETUP_RAIDDRIVES[@]}"; do
    if [[ -n "$drive" ]]; then
        disks+=", \"$drive\""
    fi
done
disks+="]"

nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko ./disko.nix --arg disks "$disks"

if [[ ! -d /mnt ]]; then
	echo "failed to format or mount disk. aborting"
	exit 1
fi

echo "==> Setting up nixos config"

mkdir -p /mnt/persist/etc

git clone http://git.disroot.org/tumble/linux-config /mnt/persist/etc/nixos
mkdir -p /mnt/persist/etc/nixos/nixos/$SETUP_HOSTNAME
cp -v disko.nix default.nix hardware-configuration.nix /mnt/persist/etc/nixos/nixos/$SETUP_HOSTNAME
pushd /mnt/persist/etc/nixos
git add .
popd

nixos-install --root /mnt --flake /mnt/persist/etc/nixos#$SETUP_HOSTNAME