#!/bin/sh
# todo: create an installer thing with is or somthing
set -e

cd $(dirname $0)
BASE=/home/nixos/Desktop/nixos
DEST=/mnt/storage/home

#sudo cp -rv $DEST/* $BASE

sudo umount -Rfvl /mnt||true
sudo rm -rf /mnt/*||true

#sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko $BASE/disko.nix
sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko ./disko.nix

sudo cp -rv $BASE $DEST
sudo nixos-install --root /mnt --flake $DEST/nixos#default
