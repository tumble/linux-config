#! /usr/bin/env nix-shell 
#! nix-shell -i bash -p fzf newt dialog bc

echo welcome to installing nixos with tumble, get a camera ready



# step 1
sudo nixos-generate-config --no-filesystems  --show-hardware-config
echo ""
echo "Take a picture of this and press any key to continue"
read -P ""

cd $(mktemp -d)
git clone http://git.disroot.org/tumble/linux-config .

SETUP_OSDRIVE=$(lsblk -Sno ID-LINK,NAME,MODEL,TRAN,SIZE|fzf --header="Select a OS Drive" --layout=reverse --ansi | awk '{print "/dev/disk/by-id/" $1}' )
SETUP_HOMEDRIVE=$(lsblk -Sno ID-LINK,NAME,MODEL,TRAN,SIZE|fzf --header="Select a Home Drive" --layout=reverse --ansi | awk '{print "/dev/disk/by-id/" $1}' )


# display partition layout based on selection
cat <<EOF
PARTITIONS

Temp FS:
nodev	tmpfs
			/tmp

OS Drive:
$(lsblk -Sno TRAN,MODEL,SIZE "$SETUP_OSDRIVE")	
	fat32	/boot		500 MB
	swap	swap		${SETUP_SWAP}G
	btfs
		/persist
		/nix
EOF

# If no home or raid drive is defined, put /home under OS DRIVE
if [[ -z "$SETUP_HOMEDRIVE"  ]]; then
echo "		/home"
fi

# Show home drive if defined
if [[ -n "$SETUP_HOMEDRIVE" ]]; then
    cat <<EOF

Home Drive:
$(lsblk -Sno TRAN,MODEL,SIZE "$SETUP_HOMEDRIVE")	
	btfs
			/home
EOF
fi


read -rp "Is this correct (Y/N)?" response

if [[ "${response,,}" == "yes" || "${response,,}" == "y" || "${response,,}" == "1" ]]; then
echo ""
echo ""
		echo "WARNING: ALL DATA ON THE FOLLING DISKS WILL BE LOST"
		lsblk -Sno ID-LINK,NAME,MODEL,TRAN,SIZE $SETUP_OSDRIVE $SETUP_HOMEDRIVE $SETUP_RAIDCACHE $SETUP_RAIDDRIVES
		read -rp "Proceed with format and nixos-install (Y/N)?" response

		if [[ "${response,,}" == "yes" || "${response,,}" == "y" || "${response,,}" == "1"  ]]; then
			echo "Continuing..."
		else
			echo "Aborting script."
			exit 0
		fi
else
    echo "Aborting script."
    exit 0
fi

echo "====> Formatting and partitioning disks using Disko..."
disks="["
disks+="\"$SETUP_OSDRIVE\""
if [[ -n "$SETUP_HOMEDRIVE" ]]; then
    disks+=", \"$SETUP_HOMEDRIVE\""
fi
if [[ -n "$SETUP_RAIDCACHE" ]]; then
    disks+=", \"$SETUP_RAIDCACHE\""
fi
for drive in "${SETUP_RAIDDRIVES[@]}"; do
    if [[ -n "$drive" ]]; then
        disks+=", \"$drive\""
    fi
done
disks+="]"

nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko ./disko.nix --arg disks "$disks"

if [[ ! -d /mnt ]]; then
	echo "failed to format or mount disk. aborting"
	exit 1
fi