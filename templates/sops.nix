{
  description = "Set up a sops file";
  path = ./sops;
  welcomeText = ''
    # Getting started
    - Run 'sops-geb' to generate the private key
    - Run 'sops-pub' to get the public key
    - Edit the .sops.yml file to add it to it
    - create any files with 'sops (file)'
  '';
}
