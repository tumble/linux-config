{
  description = "Set up a jekyll site with nix";
  path = ./jekyll;
  welcomeText = ''
    # Getting started
    - Run `nix run .#bundle`
    - Run `nix develop`
  '';
}
