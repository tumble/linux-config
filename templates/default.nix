lib:
builtins.listToAttrs (lib.lists.flatten (lib.mapAttrsToList (
  name: type:
    if name != "default.nix"
    then {
      name = lib.strings.removeSuffix ".nix" name;
      value = import (./. + "/${name}.nix");
    }
    else []
) (builtins.readDir ./.)))
