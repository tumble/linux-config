{
  writeShellScriptBin,
  stdenv,
  ...
}:
writeShellScriptBin "autorun-dvd" (builtins.readFile ../scripts/run-dvd-drive.sh)
