{stdenv, ...}:
stdenv.mkDerivation {
  name = "mudkip-wechip";
  src = ./.;
  phases = ["installPhase"];
  installPhase = ''
    mkdir -p $out
    cp -r $src/etc $out
  '';
}
