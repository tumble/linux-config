{
  description = "tumble-mudkip";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs = {
    self,
    nixpkgs,
  }: let
    system = "x86_64-linux";
    lib = nixpkgs.lib;
    pkgs = import nixpkgs {
      inherit system;
    };
  in {
    packages.${system}.mudkip-wechip = pkgs.stdenv.mkDerivation {
      name = "mudkip-wechip";
      src = ./.;
      phases = ["installPhase"];
      installPhase = ''
        mkdir -p $out/etc/udev
        cp -r $src/etc/udev/hwdb.d $out/etc/udev
      '';
    };
  };
}
