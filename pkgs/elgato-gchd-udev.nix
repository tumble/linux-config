{
  stdenv,
  fetchFromGitHub,
  ...
}:
stdenv.mkDerivation {
  name = "elgato-gchd-udev";
  src = fetchFromGitHub {
    owner = "tolga9009";
    repo = "elgato-gchd";
    rev = "master";
    sha256 = "sha256-jrTukP3pTV+V7padfZCsMvNWv4TpUgM+UfFrBZ+YN3E=";
  };
  phases = ["installPhase"];
  installPhase = ''
    mkdir -p $out/etc/udev/rules.d
    cp $src/udev-rules/55-elgato-game-capture.rules $out/etc/udev/rules.d
  '';
}
