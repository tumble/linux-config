{
  writeShellScriptBin,
  stdenv,
  ...
}:
#writeScriptBin "tumble-install" (builtins.readFile ../scripts/installer.sh)
writeShellScriptBin "tumble-mkhw" ''
  nixos-generate-config --no-filesystems --show-hardware-config
''
