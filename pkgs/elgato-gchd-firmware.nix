{
  stdenv,
  fetchzip,
  ...
}:
stdenv.mkDerivation {
  name = "elgato-gchd-firmware";
  src = fetchzip {
    url = "https://edge.elgato.com/egc/macos/egcm/2.11.14/final/Game_Capture_HD_2.11.14.zip";
    hash = "sha256-tmDZFK/kiptssVlcBhdtsxbL612M2Yntzhj6SHZLUS0=";
  };
  phases = ["installPhase"];
  installPhase = ''
    mkdir -p $out/lib/firmware/gchd
    cd $src/Contents/Resources/Firmware/Beddo
    cp mb86h57_h58_idle.bin $out/lib/firmware/gchd
    cp mb86h57_h58_enc_h.bin $out/lib/firmware/gchd
    cp mb86m01_assp_nsec_idle.bin $out/lib/firmware/gchd
    cp mb86m01_assp_nsec_enc_h.bin $out/lib/firmware/gchd
  '';
}
