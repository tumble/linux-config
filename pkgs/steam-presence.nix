{
  lib,
  fetchFromGitHub,
  python39Packages,
  stdenv,
  python39,
  writeShellScript,
  ...
}: let
  version = "1.12.1";
  src = fetchFromGitHub {
    owner = "JustTemmie";
    repo = "steam-presence";
    rev = "v${version}";
    sha256 = "sha256-8xPPvtmtx5Lq41N0FFTUZwHSkcKaH48wW9OOHEL3ON0=";
  };

  drv = {
    pip,
    requests,
    buildPythonPackage,
    ...
  }: let
    bin = let
      python = "${python39}/bin/python3";
      pipe = "${pip}/bin/pip";
    in
      writeShellScript "steam-presence"
      ''
        BASE=$(dirname $0)/..
        VENV=`mktemp -d`
        ${python} -m venv $VENV
        source $VENV/bin/activate
        pip install -r $BASE/lib/python3.9/site-packages/steam-presence/requirements.txt
        python3 $BASE/lib/python3.9/site-packages/steam-presence/main.py
      '';
  in
    buildPythonPackage {
      pname = "steam-presence";
      inherit version src;
      pyproject = false;
      installPhase = ''
        mkdir -p $out/{bin,lib/python3.9/site-packages}
        cd $out
        cp ${bin} bin/steam-presence
        cp -r $src lib/python3.9/site-packages/steam-presence
      '';
    };
in
  drv python39Packages
