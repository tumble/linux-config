{
  writeShellScript,
  stdenv,
  ...
}: let
  bin = writeShellScript "tumble-install" ''
    $(dirname $0)/../share/scripts/basic-installer.sh
  '';
in
  #writeScriptBin "tumble-install" (builtins.readFile ../scripts/installer.sh)
  stdenv.mkDerivation {
    src = ./..;
    name = "tumble-install-basic";

    buildPhase = ''
      mkdir -p $out/bin
      ln -s ${bin} $out/bin/tumble-install-basic
      ln -s $src $out/share
    '';
  }
