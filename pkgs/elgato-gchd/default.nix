{
  stdenv,
  fetchFromGitHub,
  cmake,
  libusb1,
  pkg-config,
  ...
}:
stdenv.mkDerivation {
  name = "elgato-gchd";
  src = fetchFromGitHub {
    owner = "tolga9009";
    repo = "elgato-gchd";
    rev = "master";
    sha256 = "sha256-jrTukP3pTV+V7padfZCsMvNWv4TpUgM+UfFrBZ+YN3E=";
  };
  patches = [./elgato-gchd-uint.patch];
  nativeBuildInputs = [
    cmake
    libusb1
    pkg-config
  ];
}
