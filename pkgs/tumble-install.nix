{
  writeShellScript,
  stdenv,
  ...
}: let
  bin = writeShellScript "tumble-install" ''
    $(dirname $0)/../share/scripts/installer.sh
  '';
in
  #writeScriptBin "tumble-install" (builtins.readFile ../scripts/installer.sh)
  stdenv.mkDerivation {
    src = ./..;
    name = "tumble-install";

    buildPhase = ''
      mkdir -p $out/bin
      ln -s ${bin} $out/bin/tumble-install
      ln -s $src $out/share
    '';
  }
