{
  writeScriptBin,
  libnotify,
  ...
}:
writeScriptBin "tumble-update" ''
  #!/bin/sh
  set -e
  NO_GIT_REMOTE=0
  NIX_HM=0
  FLAKE=git+https://git.disroot.org/tumble/linux-config?ref=nixos
  if [ -f $XDG_DATA_HOME/home-manager/flake.nix ]; then
      FLAKE=$XDG_DATA_HOME/home-manager
      NIX_HM=1
  fi
  if [ -f /etc/nixos/flake.nix ]; then
      FLAKE=/etc/nixos
      NIX_HM=0
  fi


  #echo "=> Cleanup"
  #sudo nix-collect-garbage


  TYPE="switch"
  while [[ $# -gt 0 ]]; do
      case $1 in
          -d|--debug)
              DEBUG=1
          ;;
          -g|--no-git)
              NO_GIT_REMOTE=1
          ;;
          -b|--boot)
              TYPE="boot"
          ;;
          -f|--flake)
              FLAKE="$2"
              shift
          ;;
          -hm|--home-manager)
              NIX_HM="$2"
              shift
          ;;
      esac
      shift
  done


  if [[ ! -w $FLAKE ]]; then
      NO_GIT_REMOTE=1
  fi

  echo "Update Options:"
  echo "   Debug mode: $( [[ -n "$DEBUG" ]] && echo Yes || echo No )"
  echo "   Use Git Remote: $( [[ "$NO_GIT_REMOTE" -eq 1 ]] && echo No || echo Yes )"
  echo "   Mode: $( [[ "$NIX_HM" -eq 1 ]] && echo "Home Manager" || echo "NixOS" )"
  echo "   Type: $TYPE"
  echo "   Flake: $FLAKE"
  echo "   Rebuild Command: $( [[ "$NIX_HM" -eq 1 ]] && echo "home-manager" || echo "sudo nixos-rebuild" ) $TYPE --flake $FLAKE"
  echo ""
  read -p "Do you accept these options? [y/N] " -n 1 -r
  if [[ ! $REPLY =~ ^[Yy]$ ]]; then
      echo "Aborting..."
      exit 1
  fi
  echo ""


  if [ -n "$DEBUG" ]; then
      set -x
  fi

  if [[ -w $FLAKE ]]; then
      pushd $FLAKE>/dev/null
      echo "=> Updating Channels..."
      nix flake update
  fi

  if [[ -w $FLAKE ]]; then
      if [[ $NO_GIT_REMOTE != 1 ]]; then
      git fetch
          echo "=> Obtaining remote changes"
          git stash
          git pull
          git stash pop
      fi
      git restore --staged .
      git add -N $(find -name '*.nix')
      echo "=> Checking for new changes to apply"
      LOCAL_DIFF=$(git diff)
      REMOTE_DIFF=""
      if [[ $NO_GIT_REMOTE != 1 ]]; then
          REMOTE_DIFF=$(git diff nixos...origin/nixos)
      fi

      if [[ -z "$LOCAL_DIFF" && -z "$REMOTE_DIFF" ]]; then
          echo "No changes detected, exiting."
          popd >/dev/null
          exit 0
      fi
      [[ $NO_GIT_REMOTE != 1 ]] && git diff -U0 nixos...origin/nixos
      git diff -U0
      git add .
  fi

  if [[ $NIX_HM = 1 ]]; then
      echo "=> Nix Home Manager Rebuilding..."
      home-manager $TYPE --flake $FLAKE --refresh --show-trace&>nixos-switch.log || (cat nixos-switch.log | grep --color error && exit 1)
      if [[ $NO_GIT_REMOTE != 1 ]]; then
          git commit -am "[home/$USER] $(home-manager generations | head -n 1 )"
      fi
  else
      echo "=> NixOS Rebuilding..."
      sudo nixos-rebuild $TYPE --flake $FLAKE --refresh --show-trace&>nixos-switch.log || (cat nixos-switch.log | grep --color error && exit 1)
      if [[ $NO_GIT_REMOTE != 1 ]]; then
          git commit -am "[nixos/$HOSTNAME] $(nixos-rebuild list-generations | grep current)"
      fi
  fi


  ${libnotify}/bin/notify-send -e "NixOS Rebuilt OK!" --icon=software-update-available
  if [[ $NO_GIT_REMOTE != 1 ]]; then
      echo "=> Pushing changes"
      git push
  fi
  popd>/dev/null
''
