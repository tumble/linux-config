# hello
```nix
config.tumble-config = {
	users = {
		tumble = { 
			state = "23.11";
			desktop = "plasma";
			terminal = "foot";
			editor = "micro";
			web = "brave";
			image = "mpv";
		};
		tv = {
			state = "23.11";
			desktop = "kodi";	 
		};
	};
	systems = {
		desktop = {
			state = "23.11";
			ip = "192.168.0.98";
			users = ["tumble"];
			programming.enable = true;
			gaming = {
				enable = true;
				vr.enable = true;
			};
		};
		tv = {
			state = "23.11";
			autologin = "tv";
			users = ["tv" "tumble" ];
			programming.enable = false;
			gaming.enable = true;
		};
	};
}
```

## end goal
two nixos systems

## Defaults
browser: brave
texteditor: vscode/lspce/kate
video: mpv
audio: mpv
image: feh
terminal: foot

## Folder structure
config.nix - users and systems and program setup
lib
  - homeManager - generate set of home-manager configs
  - nixos - generate set of hosts
modules
  - common stuff here
  - folder
    - folders are modules

## whats the exact plan for modules ??
hmm, maybe 
```nix
{config, ...}: {
	options = {
	};
	home = {}: {};
}
```

## libs
* default.nix - import the libs
* pkgs.nix import the pkgs
* modules.nix - home and nixos configs
* configs.nix - home and nixos configs

## module
options: module options
nixos: {pkgs, config, ...}: {}
home: {pkgs, config, ...}: {}

## steps
1 setup lib
2 setup pkgs
3 setup modules

* lib
files to {file:include}
* pkgs
files to system={file=include}

* 

## noteable funtions
lib.path.append - safer than path + "/" + path
lib.filesystem.pathType "regular" or "directory" or "symlink” or "unknown"
lib.filesystem.pathIsDirectory - is a folder
lib.filesystem.listFilesRecursive  - list file tree in flattened array
