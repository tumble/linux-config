{
  pkgs,
  lib,
  inputs,
  self,
  config,
  osConfig ? null,
  ...
}: {
  programs.home-manager.enable = true;

  home.stateVersion = "23.11";
  home.username = "tv";

  home.persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
    "/storage${config.home.homeDirectory}".directories = [
      ".kodi"
    ];
  };
}
