{
  pkgs,
  inputs,
  self,
  lib,
  config,
  osConfig ? null,
  ...
}: {
  programs.home-manager.enable = true;

  home.stateVersion = "23.11";
  home.username = "tumble";

  home.file.".face.icon".source = ./tumble.png;

  programs.fish.enable = true;
  programs.bash.enable = true;

  home.persistence = lib.mkIf (osConfig.tumble-config.impermanence.home.enable or false) {
    "/storage${config.home.homeDirectory}" = let
      configHome = ".config";
      dataHome = ".local/share";
    in {
      directories = [
        "${configHome}/nheko"
        "${configHome}/keepassxc"
        "${configHome}/feishin"
        {
          directory = ".local/var/pmbootstrap";
          method = "symlink";
        }
        {
          directory = "${configHome}/discordcanary";
          method = "symlink";
        }
        "${dataHome}/nheko"
        "Sync"
        ".ssh"
        ".mozilla"
        ".librewolf"
        ".config/spotify"
        ".bash_history"
      ];
    };
  };
}
