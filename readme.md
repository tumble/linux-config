## installing
use the repo url "git+https://git.disroot.org/tumble/linux-config?ref=nixos" or clone the repository to a local directory

- if you're using this as a nix system config, run:
	`sudo nixos-rebuild switch --flake <repo>`
- if you're using this as a home manager config, run:
	`home-manager --extra-experimental-features "nix-command flakes" switch --flake <repo>`

## updating
if you've pulled from the url, you can pull updates by running the install command again with `--refresh`.
if you have a local repo, you can just `git pull`, or make any other changes you want, then run the install command.

## modifying
machine and user configuration is done in flake.nix.
nixos configuration is done by hostname, and home configuration is done by username.

if you want to override the hostname or user to install as, add `#<host>` to the install path.

## setting up nixos
> todo: turn the scripts/server-install into generic instal steps
create a disko config
run the disko config through the nix-comunity flake to format and moun the disks for you
run `nixos-generate-config --no-filesystems` and obtain the state version from configuration.nix and get the hardware-configuration.nix file from the generated `/mnt/etc/nixos`, or just use `--show-hardware-config`

## things that don't work
### impermanence
* had to fight with it until syncthing worked
### hyprland
* stuck to one workspace without setting up binds to put thing in workspace
* no bar by default
### gnome
* its ok
* it works
### librewolf
* no matter what way you install it, it's missing its config and the extension store doesnt work
### lapce
* the "workspace tabs" (tabs that seem to be above everything), only appear if you run `lapce <dir>` while lapce is open
	* and then there's a + for creating more
* the file explorer only lets you open files and not delete, move, or create files and folders
* discard changes does work
### vscode
* really good, but its electron
### sddm
* looks bad when you dont also have plasma enabled
### kate
* it was rigid in some way, i don't remember
* also setting up plugins is nearly as complicated as seting up dependencies in c++ visual studio community



## TODO
make fcitx5 optional
fix ssh key stuff